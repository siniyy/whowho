(() => {
  'use strict';

  document.addEventListener('scroll', scroll);

  function scroll() {
    const body = document.querySelector('body');

    if (body.offsetWidth <= 960) {
      return;
    }

    const top = (window.pageYOffset || document.scrollTop || 0)  - (document.clientTop || 0);

    document.querySelectorAll('.main-images img').forEach((img) => {
      const coefficient = img.getAttribute('data-coefficient');

      img.style.marginTop = top / (13 - coefficient);
    });
  }
})();
