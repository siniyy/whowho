'use strict';

const path = require('path');
const fs = require('fs');
const Koa = require('koa');
const Router = require('koa-router');
const app = new Koa();
const router = new Router();
const koaStatic = require('koa-static');


app.use(koaStatic(path.join(__dirname, 'www')));

router.get('/', (ctx, next) => {
  const filepath = path.join(__dirname, 'www/index.html');

  ctx.type = 'text/html';
  ctx.body = fs.createReadStream(filepath);
});

app
  .use(router.routes())
  .use(router.allowedMethods());


app.listen(80, () => console.log(`Listening on port 80`));
