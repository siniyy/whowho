'use strict';

const IO = require('koa-socket');
const io = new IO();

const config = require('config');

const TopicsController = require('../controllers/topic');
const CommentsController = require('../controllers/comment');

let socketIO;

function setConnect(app) {
  app._io.on(config.socket.CONNECTION, (socket) => {
    socket.emit(config.socket.SUCCESS_CONNECTION);

    setSocketEvents(socket);
  });
}

function setSocketEvents(socket) {
  const userId = socket && socket.handshake && socket.handshake.headers && socket.handshake.headers.user_id;

  if (!userId) {
    return;
  }

  const user = {
    _id: userId,
    name: socket.handshake.headers.user_name,
    pic: socket.handshake.headers.user_pic
  };

  socket.on(config.socket.DISCONNECT, () => {
    socket.leaveAll();
  });

  socket.on(config.socket.OPEN_TOPIC, async(data) => {
    socket.join(data.topic_id);

    const response = await TopicsController.openTopic(user, data);

    socket.emit(config.socket.OPEN_TOPIC, sendDataToClient(response, data));
  });

  socket.on(config.socket.CLOSE_TOPIC, async(data) => {
    socket.leave(data.topic_id);
    await TopicsController.closeTopic(user, data);
  });

  socket.on(config.socket.NEW_COMMENT, async(data) => {
    const comment = await CommentsController.create(user, data);
    sendNewComment(comment, data, user);
  });

  socket.on(config.socket.UPDATE_COMMENT, async(data) => {
    const comment = await CommentsController.update(user, data);
    socketIO.to(data.topic_id).emit(config.socket.UPDATE_COMMENT, sendDataToClient(comment, data));
  });

  socket.on(config.socket.REMOVE_COMMENT, async(data) => {
    await CommentsController.remove(user, data);
    socketIO.to(data.topic_id).emit(config.socket.REMOVE_COMMENT, sendDataToClient({_id: data.comment_id}, data));
  });

  socket.on(config.socket.COMMENT_LIKE, async(data) => {
    const count = await CommentsController.like(user, data);
    socketIO.to(data.topic_id).emit(config.socket.COMMENT_LIKE, sendDataToClient({_id: data.comment_id, count, type: data.type}, data));
  });

  socket.on(config.socket.GET_COMMENTS, async(data) => {
    const comments = await CommentsController.findComments(Object.assign({user_id: user._id}, data));
    socket.emit(config.socket.GET_COMMENTS, sendDataToClient({comments}, data));
  });

  socket.on(config.socket.TOPICS_NOTIFICATIONS_OPEN, async(data) => {
    data.topics.forEach((topic_id) => {
      socket.join(`${config.socket.NEW_COMMENT_NOTIFICATION}_${topic_id}`);
    });
  });

  socket.on(config.socket.TOPICS_NOTIFICATIONS_CLOSE, async(data) => {
    data.topics.forEach((topic_id) => {
      socket.leave(`${config.socket.NEW_COMMENT_NOTIFICATION}_${topic_id}`);
    });
  });
}

function sendNewComment(comment, data, user) {
  const userData = {
    _id: user._id,
    name: user.name,
    pic: user.pic
  };

  socketIO.to(data.topic_id).emit(config.socket.NEW_COMMENT, Object.assign(sendDataToClient(comment, data), {user: userData}));

  const notificationData = sendDataToClient({
    topic_id: data.topic_id,
    created_at: comment.created_at,
    text: comment.text,
    images: comment.images
  }, data);

  socketIO.to(`${config.socket.NEW_COMMENT_NOTIFICATION}_${data.topic_id}`).emit(config.socket.NEW_COMMENT_NOTIFICATION, notificationData);
}

function sendDataToClient(data, socketData) {
  return Object.assign({}, data, {topic_id: socketData.topic_id, random_client_id: socketData.random_client_id});
}

module.exports = {
  sendNewComment,
  setSocketIO(app) {
    io.attach(app);

    socketIO = app._io;

    setConnect(app);
  }
};