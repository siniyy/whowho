'use strict';

const winston = require('winston');
const path = require('path');

function getLogger(module, fileName) {
  const transports = [

    new winston.transports.Console({
      timestamp: true,
      colorize: true,
      level: 'debug',
      label: module.filename.split('/').slice(-2).join('/')
    }),

    new winston.transports.File({
      filename: path.join(__dirname, `/../logs/${fileName || 'error'}.log`),
      level: 'info'
    })
  ];

  return new winston.Logger({ transports });
}

module.exports = getLogger;
