const config = require('config');
const mailer = require('./mailer');
const initialData = require('./helpers/initialData');

module.exports = notifyModerators;

const resources = {
  topics: {
    name: 'topic',
  },
  categories: {
    name: 'category',
  },
};

async function notifyModerators(type, data) {
  const resource = resources[type];

  const title = data.title.length > 50 ? data.title.slice(0, 50) + '...' : data.title;

  await mailer({
    to: initialData.getModerators(),
    subject: `New ${resource.name}: ${title}`,
    html: `<b>Title: </b>${data.title}. <br><a href="${config.uri}/admin/${type}/edit/${data._id}">Review</a>`,
  });
}