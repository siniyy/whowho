'use strict';

const Category = require('./../../models/category');
const User = require('./../../models/user');

const data = {};

async function setCategories() {
  const allCategories = await Category.find({}, 'title pic category_id').sort({rating: -1}).lean();

  data.categories = allCategories.map(c => Object.assign(c, {sub: addSubCategories(c, allCategories)}));

  data.breadCrumbs = setBreadCrumbs(data.categories);
}

function addSubCategories(category, allCategories) {
  return allCategories
    .filter(c => category._id.equals(c.category_id))
    .map((subC) => Object.assign(subC, {sub: addSubCategories(subC, allCategories)}));
}

function setBreadCrumbs(categories) {
  const breadCrumb = {};

  categories.forEach(c => setForCategory(c));

  return breadCrumb;

  function setForCategory(c, categoryId) {
    breadCrumb[c._id] = categoryId ? breadCrumb[categoryId].slice(0) : [];
    breadCrumb[c._id].push({_id: c._id, title: c.title});

    c.sub.forEach(category => setForCategory(category, c._id));
  }
}

function getCategories(filter) {
  const categories = data
    .categories
    .filter(c => !filter.main || !c.category_id)
    .filter(c => !filter._id || c._id.equals(filter._id))
    .filter(c => !filter.title || c.title.toLowerCase().includes(filter.title.toLowerCase()))
    .map((c) => {
      const copied = Object.assign({}, c);

      if (!filter.sub) {
        delete copied.sub;
      }

      return copied;
    });

  if (filter._id) {
    return categories.length ? categories[0] : null;
  }

  return categories;
}

function getBreadCrumbs(categoryId) {
  return categoryId ? data.breadCrumbs[categoryId] : data.breadCrumbs;
}

async function setModerators() {
  data.moderators = await User
  .find({ isModerator: true }, 'email')
  .then(moderators => moderators
    .map(moderator => moderator.email)
    .join()
  );
}

function getModerators() {
  return data.moderators;
}

// FIXME move to global provider
module.exports = {
  setCategories,
  getCategories,
  getBreadCrumbs,

  setModerators,
  getModerators,
};