'use strict';

const APNS = require('apns2');
const BasicNotification = APNS.BasicNotification;
const fs = require('mz/fs');
const path = require('path');
const config = require('config');

let client;

async function setConnection() {
  const certificate = await fs.readFile(path.join(__dirname, `./certificate/${config.apns.certificate}`));

  client = await new APNS({
    host: config.apns.host,
    team: config.apns.teamId,
    keyId: config.apns.keyId,
    signingKey: certificate,
    defaultTopic: config.apns.bundleId
  });
}

async function send(deviceToken, data) {
  const bn = new BasicNotification(deviceToken, data.message, {alert: data});

  return client
    .send(bn)
    .catch((err) => {
      console.log(`Error send push to ui: ${err.reason}`);
    });
}

exports.setConnection = setConnection;
exports.send = send;