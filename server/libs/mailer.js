'use strict';

const nodemailer = require('nodemailer');
const logger = require('./logger')(module);

// FIXME add real account
// const transporter = nodemailer.createTransport('smtps://mobile-test@ironsrc.com:yiigakxjtitnpwkg@smtp.gmail.com');
const transporter = nodemailer.createTransport('smtps://semenyuk.odesk@gmail.com:miomihwunqcvthjd@smtp.gmail.com');

const mailOptions = {
  from: 'WhoWho <info@whowho.com>'
};

function sendMail(options) {
  return new Promise((resolve, reject) => {
    transporter.sendMail(Object.assign(mailOptions, options), (err, info) =>{
      if (err) {
        return reject(err);
      }

      resolve(info);
    });
  }).catch(logger.error);
}

module.exports = sendMail;