'use strict';

const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, './config');

const config = require('config');
const Koa = require('koa');
const app = new Koa();

const initialData = require('./libs/helpers/initialData');
const apns = require('./libs/apns');

app.keys = [config.secret];

// mongoose
require('./libs/mongoose');

// include middlewares
require('./middlewares')(app);

// include routes
require('./routes')(app);

// set sockets
require('./libs/socket').setSocketIO(app);

const port = process.env.PORT || 8888;

run();

async function run() {
  await initialData.setCategories();
  await initialData.setModerators();

  await apns.setConnection();

  app.listen(port, () => console.log(`Listening on port ${port}`));
}
