'use strict';

const mongoose = require('mongoose');

const topicSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },

  pic: String,

  data: [{
    type: {
      type: String
    },
    data: String,
    _id: {
      type: mongoose.Schema.Types.Mixed,
      ref: 'Poll'
    }
  }],

  likes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],

  likes_qty: {
    type: Number,
    default: 0
  },

  created_at: {
    type: Date,
    default: Date.now
  },

  updated_at: {
    type: Date,
    default: Date.now
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  category_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },

  last_comment_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comment'
  },

  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],

  active_users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],

  moderated: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model('Topic', topicSchema);