'use strict';

const mongoose = require('mongoose');

const pollSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['single', 'vs']
  },

  text: String,

  options: [{
    text: String,
    pic: String,
    qty: {
      type: Number,
      default: 0
    }
  }],

  created_at: {
    type: Date,
    default: Date.now
  },

  end_date: Date
});

module.exports = mongoose.model('Poll', pollSchema);