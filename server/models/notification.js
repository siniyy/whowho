'use strict';

const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
  notifications_qty: {
    type: Number,
    default: 0
  },

  comment_id: { // last seen comment id
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comment'
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  topic_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Topic'
  }
});

module.exports = mongoose.model('Notification', notificationSchema);