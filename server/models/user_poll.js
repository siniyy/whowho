'use strict';

const mongoose = require('mongoose');

const userPollSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  poll_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Poll',
    required: true
  },

  option_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  }
});

userPollSchema.index({user_id: 1, poll_id: 1}, {unique: true});

module.exports = mongoose.model('user_poll', userPollSchema);