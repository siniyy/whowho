'use strict';

const mongoose = require('mongoose');
const findOrCreate = require('mongoose-findorcreate');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },

  email: String,
  gender: String,
  location: String,
  birthday: String,
  pic: String,
  facebook_id: String,
  google_id: String,
  apple_device_ids: {
    type: [String],
    default: []
  },

  categories: [{
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    },

    targeted_at: Date
  }],

  topics: [{
    updated_at: {
      type: Date,
      default: Date.now
    },

    _id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Topic'
    },

    isTargeted: Boolean,
    isHistory: Boolean,
    isDirect: Boolean,
    isFavorite: Boolean
  }],

  seenTopics: Object,

  notInterested: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Topic'
  }],

  likes_qty: {
    type: Number,
    default: 0
  },

  is_admin: {
    type: Boolean,
    default: false,
  },

  isBlocked: {
    type: Boolean,
    default: false,
  },

  isModerator: {
    type: Boolean,
    default: false,
  },

  created_at: {
    type: Date,
    default: Date.now
  },
});

userSchema.plugin(findOrCreate);

module.exports = mongoose.model('User', userSchema);