'use strict';

const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },

  pic: String,

  rating: {
    type: Number,
    default: 0
  },

  posts_qty: {
    type: Number,
    default: 0
  },

  category_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    default: null
  },

  topics: [{
    updated_at: {
      type: Date,
      default: Date.now
    },

    created_at: {
      type: Date,
      default: Date.now
    },

    _id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Topic'
    }
  }],

  newest: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Topic'
  }],

  popular: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Topic'
  }]
});

module.exports = mongoose.model('Category', categorySchema);