'use strict';

const mongoose = require('mongoose');
const deepPopulate = require('mongoose-deep-populate')(mongoose);

const commentSchema = new mongoose.Schema({
  text: String, // required or images
  images: [String], // required or text

  created_at: {
    type: Date,
    default: Date.now
  },

  likes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],

  comment_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comment',
    default: null
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  topic_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Topic',
    required: true
  },

  is_edited: {
    type: Boolean,
    default: false
  },

  moderated: {
    type: Boolean,
    default: false,
  },
});

commentSchema.plugin(deepPopulate, {
  populate: {
    'user_id': {
      select: ['name', 'pic']
    },
    'comment_id.user_id': {
      select: ['name', 'pic']
    }
  }
});

module.exports = mongoose.model('Comment', commentSchema);