'use strict';

const config = require('config');
const hooks = require('../hooks');

const Comments = require('./../models/comment');
const Notifications = require('./../models/notification');
const Topics = require('./../models/topic');
const Users = require('./../models/user');

const UsersController = require('./../controllers/user');

async function findComments({user_id, topic_id, comment_id, direction, first_open}) {
  const commentsOnPage = config.commentsOnPage;
  const last = [];

  // find comments ids
  const data = await Comments
    .find({topic_id}, '_id')
    .sort('created_at')
    .lean()
    .then((comments) => {
      last.push(...comments.slice(-commentsOnPage));

      if (comment_id) { // last seen comment OR last comment on UI for direction
        const index = comments.findIndex(c => c._id.equals(comment_id));

        // SCROLL
        // ${commentsOnPage} prev or next
        if (direction) {
          let amount = commentsOnPage;

          // next
          if (direction === 'next') {
            return {
              list: comments.splice(index + 1, amount), // + 1 => not to return current
              next: !!comments[index + 1]
            };
          }

          // prev
          let startIndex = index - commentsOnPage;
          let prev = true;

          // if prev comments amount is less than "half of comments on page" -- fetch ${commentsOnPage} from very first comment
          if (index < commentsOnPage) {
            prev = false;

            startIndex = 0;
            amount = index;
          }

          return {
            list: comments.splice(startIndex, amount),
            prev
          };
        }

        // OPEN
        // ${commentsOnPage / 2} prev and ${commentsOnPage / 2} next
        const halfCommentsOnPage = commentsOnPage / 2;

        let startIndex = index - halfCommentsOnPage;
        let amount = commentsOnPage;
        let prev = true;

        // if prev comments amount is less than "half of comments on page" -- fetch ${commentsOnPage} from very first comment
        if (index < halfCommentsOnPage) {
          prev = false;

          startIndex = 0;
          amount = commentsOnPage;
        }

        return {
          list: comments.splice(startIndex, amount),
          prev,
          next: !!comments[startIndex]
        };
      }


      // if user opens topic with comments for the first time -- show latest
      if (first_open) {
        return {
          list: comments.splice(-commentsOnPage),
          prev: !!comments.length
        };
      }

      // user opens topic without comments, close, and re-open after N comments added -- show from first
      return {
        list: comments.splice(0, commentsOnPage),
        next: !!comments.length
      };
    });

  // fetch and transform comments for ui
  data.list = await getCommentsListByIds(user_id, data.list.map(i => i._id));

  if (data.next) {
    data.last = await getCommentsListByIds(user_id, last.map(i => i._id));
  }

  return data;
}

function getCommentsListByIds(user_id, ids) {
  return Comments
    .find({
      _id: {
        $in: ids
      }
    }, '-__v -topic_id')
    .sort('created_at')
    .deepPopulate('user_id comment_id comment_id.user_id')
    .lean()
    .then(comments => comments.map(comment => changeCommentForShow(user_id, comment)));
}

function changeCommentForShow(userId, comment, isReply) {
  if (!isReply) {
    comment.likes_qty = comment.likes.length;
    comment.is_liked = comment.likes_qty ? comment.likes.some(likeId => likeId.equals(userId)) : false;

    if (comment.comment_id) {
      comment.reply = changeCommentForShow(comment.comment_id.user_id._id, comment.comment_id, true);
    }
  }

  comment.user = comment.user_id;

  // delete comment.user_id; // fixme need to investigate why error
  delete comment.comment_id;
  delete comment.likes;

  return comment;
}

async function createRest(ctx) {
  const body = ctx.request.body;
  const files = ctx.request.files;
  const user = await Users.findById(ctx.state.user._id);

  if (body.images && files) {
    body.images = body.images
      .map((image) => {
        return files[image] && files[image].name;
      })
      .filter(image => image);
  }

  const comment = await create(user, body);

  ctx.data = {_id: comment._id};

  ctx.socketData = {
    method: 'sendNewComment',
    data: [comment, body, user]
  };
}

async function create(user, data) {
  const createdComment = await Comments.create(Object.assign(data, {user_id: user._id}));

  await hooks(config.hooks.TOPIC_UPDATE, {topic_id: createdComment.topic_id, last_comment_id: createdComment._id});

  const comment = await Comments
    .findById(createdComment._id)
    .deepPopulate('user_id comment_id comment_id.user_id')
    .lean()
    .then(res => changeCommentForShow(user._id, res));

  await hooks(config.hooks.COMMENT_CREATE, {topic_id: createdComment.topic_id});

  if (comment.reply) {
    await hooks(config.hooks.TOPIC_ADD_TO_DIRECT, {topic_id: createdComment.topic_id, user_id: comment.reply.user._id});

    const topic = await Topics.findById(createdComment.topic_id);

    const pushData = {
      message: comment.text,
      title: `${comment.user.name}@${topic.title}`,
      topic_id: createdComment.topic_id,
      comment_id: createdComment._id,
      user_id: comment.reply.user._id
    };

    await UsersController.sendPush(pushData);
  }

  return comment;
}

async function update(user, data) {
  const comment = Object.assign(data, {is_edited: true});

  await Comments.update({_id: comment._id}, comment);

  return comment;
}

async function remove(user, data) {
  await Comments.remove({_id: data.comment_id});
}

async function like(user, data) {
  const type = data.type === 'like' ? '$addToSet' : '$pull';

  // update user total likes quantity
  await Users
    .update({
      _id: data.user_id
    }, {
      $inc: {
        likes_qty: data.type === 'like' ? 1 : -1
      }
    });

  return Comments
    .update({
      _id: data.comment_id
    }, {
      [type]: {
        likes: user._id
      }
    })
    .then(() => Comments
      .findById(data.comment_id, 'likes')
      .then(res => res.likes.length)
    );
}

async function updateNotificationCount(data) {
  const comment = await Comments.findById(data.comment_id);

  if (!comment) {
    return;
  }

  const notifications_qty = await Comments
    .count({
      topic_id: comment.topic_id,
      created_at: {
        $gt: comment.created_at
      }
    });

  await Notifications
    .update({
      topic_id: comment.topic_id,
      user_id: comment.user_id
    }, {
      $set: {
        comment_id: comment._id,
        notifications_qty
      }
    });
}

module.exports = {
  findComments,
  createRest,
  create,
  update,
  remove,
  like,
  updateNotificationCount
};
