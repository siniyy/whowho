'use strict';

const config = require('config');
const hooks = require('../hooks');

const User = require('./../models/user');

const apns = require('./../libs/apns');

async function index(ctx) {
  ctx.data = await User.find();
}

async function show(ctx) {
  ctx.data = await User
    .findById(ctx.params.id)
    .lean()
    .then((user) => {
      user.categories = user.categories.map(c => c._id);
      return user;
    });
}

async function create(ctx) {
  ctx.data = await User.create(changeUserBody(ctx.request.body, ctx.request.files));
}

async function update(ctx) {
  const data = changeUserBody(ctx.request.body, ctx.request.files);

  await User.update({_id: ctx.params.id}, data);

  ctx.data = Object.assign({}, {_id: ctx.params.id}, data);
}

async function destroy(ctx) {
  const user = await User.findById(ctx.params.id);
  await hooks(config.hooks.USER_REMOVE, user);

  await User.remove({_id: ctx.params.id});
}

function changeUserBody(body, files) {
  if (body.pic) {
    body.pic = files && files[body.pic] && files[body.pic].name;
  }

  if (body.categories) {
    body.categories = body.categories.map(_id => ({_id}));
  }

  // avoid malware request
  if (body.is_admin) {
    delete body.is_admin;
  }

  return body;
}

async function addAppleDeviceId(ctx) {
  await User
    .update({
      _id: ctx.state.user._id
    }, {
      $addToSet: {
        apple_device_ids: ctx.request.body.apple_device_id
      }
    });
}

async function sendPush(pushData) {
  const user = await User.findById(pushData.user_id);

  for (let apple_device_id of (user.apple_device_ids || [])) {
    await apns.send(apple_device_id, pushData); //todo images
  }
}

async function logout(ctx) {
  if (!ctx.request.body.apple_device_id) {
    return;
  }

  await User
    .update({
      _id: ctx.state.user._id
    }, {
      $pull: {
        apple_device_ids: ctx.request.body.apple_device_id
      }
    });
}

module.exports = {
  index,
  show,
  create,
  update,
  destroy,
  addAppleDeviceId,
  sendPush,
  logout
};