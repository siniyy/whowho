'use strict';

const config = require('config');
const hooks = require('../hooks');
const Poll = require('./../models/poll');

async function destroy(ctx) {
  await Poll.remove({_id: ctx.request.body._id});
  await hooks(config.hooks.POLL_REMOVE, ctx.request.body._id);
}

async function vote(ctx) {
  const poll_id = ctx.request.body.poll_id;
  const option_id = ctx.request.body.option_id;
  const user_id = ctx.state.user._id;

  await hooks(config.hooks.POLL_VOTE, {
    poll_id,
    option_id,
    user_id
  });

  await Poll.update({
    _id: poll_id,
    'options._id': option_id
  }, {
    $inc: {
      'options.$.qty': 1
    }
  });

  ctx.data = await Poll.findById(poll_id, {options: 1, text: 1}).lean();
}

module.exports = {
  vote
};