'use strict';

const config = require('config');
const hooks = require('../hooks');

const Topic = require('./../models/topic');
const Poll = require('./../models/poll');
const UserPoll = require('./../models/user_poll');
const User = require('./../models/user');
const Category = require('./../models/category');
const Notifications = require('./../models/notification');

const initialData = require('./../libs/helpers/initialData');

async function index(ctx) {
  let topics;

  if (ctx.query.cat_id) {
    const category = await Category.findById(ctx.query.cat_id);

    if (!category) {
      ctx.throw(`Category doesn't exist`)
    }

    topics = category.topics;
  } else {
    topics = await Topic.find({}, 'updated_at');
  }

  const user = await User.findById(ctx.state.user._id).lean();

  const page = +(ctx.query.page || 1);

  ctx.data = await getTopicsList(user, topics, page, {text: ctx.query.search});
}

async function explore(ctx) {
  const user = await User.findById(ctx.state.user._id).populate('categories._id').lean();

  const userTopicsIds = user.topics.filter(t => t.isTargeted).sort(sortByTime).map(t => t._id);
  const popularTopicIds = getCategoriesTopicsForShow(user, userTopicsIds, 'popular');
  const newestTopicIds = getCategoriesTopicsForShow(user, userTopicsIds, 'newest');

  const page = +(ctx.query.page || 1);

  const seenTopics = user.seenTopics || {};

  const idsForShow = getIdsForShow(page, user.notInterested, seenTopics, userTopicsIds, popularTopicIds, newestTopicIds);

  ctx.data = await Topic
    .find({
      _id: {
        $in: idsForShow
      }
    })
    .lean()
    .then(topics => topics.map(topic => changeTopicForShow(user, topic)));

  idsForShow.forEach((id) => {
    seenTopics[id] = seenTopics[id] || {count: 0};
    seenTopics[id].updated_at = new Date();
    seenTopics[id].count++;
  });

  await User.update({_id: user._id}, {seenTopics});
}

function getIdsForShow(page, notInterested, seenTopics, userTopicsIds, popularTopicIds, newestTopicIds) {
  const allIds = getSortedArray([userTopicsIds, popularTopicIds, newestTopicIds])
    .filter(id => !notInterested.find(nId => nId.equals(id)) && (!seenTopics[id] || seenTopics[id].count < config.maxTopicShowCount));

  return allIds.slice((page - 1) * config.topicsOnPage, page * config.topicsOnPage);
}

function getCategoriesTopicsForShow(user, userTopicsIds, type) {
  const ids = user.categories.map(c => c._id[type].filter(t => !userTopicsIds.includes(t)));
  return getSortedArray(ids);
}

function getSortedArray(arrayOfIds) {
  const res = [];

  const maxLength = Math.max(...arrayOfIds.map(a => a.length));

  for (let i = 0; i < maxLength; i++) {
    for (let a = 0; a < arrayOfIds.length; a++) {
      if (arrayOfIds[a][i]) {
        res.push(arrayOfIds[a][i]);
      }
    }
  }

  return res;
}

async function history(ctx) {
  ctx.data = await getTopicsByType(ctx, 'isHistory');
}

async function direct(ctx) {
  ctx.data = await getTopicsByType(ctx, 'isDirect');
}

async function favorite(ctx) {
  ctx.data = await getTopicsByType(ctx, 'isFavorite');
}

async function getTopicsByType(ctx, type) {
  const user = await User.findById(ctx.state.user._id).lean();

  const userTopics = user.topics.filter(t => t[type]);

  const page = +(ctx.query.page || 1);

  return getTopicsList(user, userTopics, page, {text: ctx.query.search});
}

function getTopicsList(user, userTopics, page, search = {}) {
  const topicIds = userTopics.sort(sortByTime)
    .slice((page - 1) * config.topicsOnPage, page * config.topicsOnPage)
    .map(item => item._id);

  const where = {
    _id: {
      $in: topicIds
    }
  };

  if (search.text) {
    where.title = new RegExp(search.text, 'i');
  }

  return Topic
    .find(where)
    .populate('last_comment_id', {created_at: 1, text: 1, images: 1})
    .sort({
      updated_at: -1
    })
    .lean()
    .then((topics) => {
      const topicIds = topics.map(t => t._id);

      return Notifications
        .find({
          user_id: user._id,
          topic_id: {
            $in: topicIds
          }
        })
        .then(notifications => topics.map(topic => changeTopicForShow(user, topic, notifications)));
    });
}

async function show(ctx) {
  ctx.data = await changeTopicFullData(ctx.state.user._id, ctx.params.id);
}

async function changeTopicFullData(userId, topicId) {
  const user = await User.findById(userId).lean();

  const topic = await Topic
    .findById(topicId)
    .populate('data._id', '-__v -created_at')
    .lean();

  for (let row of topic.data) {
    if (row._id) {
      row.data = row._id;

      row.data.voted = await UserPoll // set voted for poll
        .findOne({
          poll_id: row.data._id,
          user_id: userId
        })
        .then(res => res ? res.option_id : false);
    }

    delete row._id;
  }

  return changeTopicForShow(user, topic);
}

function changeTopicForShow(user, topic, notifications = []) {
  topic.isFavorited = user.topics.some(c => c.isFavorite && c._id.equals(topic._id));
  topic.isLiked = topic.likes.some(likeId => likeId.equals(user._id));
  topic.categories = initialData.getBreadCrumbs(topic.category_id);
  topic.likes_qty = topic.likes.length;
  topic.isPoll = topic.data.some(d => d.type === config.dataTypes.POLL);
  topic.last_comment = topic.last_comment_id;

  const notification = notifications.find(n => n.topic_id.equals(topic._id));

  topic.notifications_qty = notification ? notification.notifications_qty : 0;

  delete topic.likes;
  delete topic.category_id;
  delete topic.__v;
  delete topic.last_comment_id;

  return topic;
}

async function create(ctx) {
  ctx.request.body = await changeBody(ctx.request.body, ctx.request.files);

  let data = ctx.request.body;

  if (!data.fromAdmin) {
    data.user_id = ctx.state.user._id;
  }

  const targetedUsers = [];
  const usersFilteredByTime = [];

  await User //FIXME targeting(age, gender)
    .find({
      _id: {
        $ne: ctx.state.user._id
      },
      'categories._id': data.category_id
    }, {
      'categories.$._id': 1
    })
    .lean()
    .then((res) => {
      res.forEach((user) => {
        // user was targeted more than "config.targetedTime" ago
        const targetedAt = user.categories[0].targeted_at;
        if (!targetedAt || Date.now() - config.targetedTime > new Date(targetedAt).getTime()) {
          targetedUsers.push(user);
        } else {
          usersFilteredByTime.push(user);
        }
      });
    });


  const users = targetedUsers.length < config.targetCount
    ? targetedUsers.concat(getRandomUsers(usersFilteredByTime, config.targetCount - targetedUsers.length))
    : getRandomUsers(targetedUsers, config.targetCount);

  data.users = users.map(item => item._id).concat(data.user_id);

  const topic = await Topic.create(data);

  if (data.fromAdmin) {
    await hooks(config.hooks.TOPIC_MODERATE, topic);
  } else {
    await hooks(config.hooks.TOPIC_CREATE, topic);
  }

  ctx.data = await changeTopicFullData(data.user_id, topic._id);
}

function getRandomUsers(users, neededCount) {
  if (users.length <= neededCount) {
    return users;
  }

  const randomUsers = [];

  for (let i = 0; i < neededCount; i++) {
    const index = Math.floor(Math.random() * users.length);

    randomUsers.push(users[index]);

    users.splice(index, 1);
  }

  return randomUsers;
}

async function update(ctx) {
  const data = await changeBody(ctx.request.body, ctx.request.files);

  const topic = await Topic.findById(ctx.request.body._id);
  const category_id = await Category
    .findById(topic.category_id)
    .then(res => res && res._id);

  await Topic.update({_id: ctx.params.id}, data);
  await hooks(config.hooks.TOPIC_UPDATE, {topic_id: ctx.params.id, category_id });

  ctx.data = Object.assign({}, {_id: ctx.params.id}, data);
}

async function destroy(ctx) {
  await hooks(config.hooks.TOPIC_REMOVE, {topic_id: ctx.params.id});
  await Topic.remove({_id: ctx.params.id});
}

async function topicFavorite(ctx) {
  if (!ctx.request.body.topic_id) {
    ctx.throw(`Topic id is required`)
  }

  if (ctx.request.body.type === 'add') {
    await hooks(config.hooks.TOPIC_ADD_TO_FAVORITE, getUserTopicData(ctx));
  } else {
    await hooks(config.hooks.TOPIC_REMOVE_FROM_FAVORITE, getUserTopicData(ctx));
  }
}

async function notInterested(ctx) {
  if (!ctx.request.body.topic_id) {
    ctx.throw(`Topic id is required`)
  }

  await hooks(config.hooks.TOPIC_NOT_INTERESTED, getUserTopicData(ctx));
}

async function openTopic(user, data) {
  if (data.comment_id) {
    await CommentCtrl.updateNotificationCount(Object.assign({}, {user_id: user._id}, data));
  }

  const topic = await changeTopicFullData(user._id, data.topic_id);

  const notifications = await hooks(config.hooks.TOPIC_OPEN, {user_id: user._id, topic_id: data.topic_id});

  // result => {list, prev, next}
  const comments = await CommentCtrl.findComments({
    user_id: user._id,
    topic_id: data.topic_id,
    comment_id: notifications.comment_id,
    first_open: notifications.first_open
  });

  return Object.assign({}, notifications, comments, {topic});
}

async function closeTopic(user, data) {
  await hooks(config.hooks.TOPIC_CLOSE, {
    user_id: user._id,
    topic_id: data.topic_id,
    notifications_qty: data.notifications_qty,
    comment_id: data.comment_id
  });
}

async function like(ctx) {
  const type = ctx.request.body.type;
  const insertionType = type === 'like' ? '$addToSet' : '$pull';
  const incValue = type === 'like' ? 1 : -1;

  await Topic
    .update({
      _id: ctx.request.body.topic_id
    }, {
      [insertionType]: {
        likes: ctx.state.user._id
      },
      $inc: {
        likes_qty: incValue
      }
    });
}

function getUserTopicData(ctx) {
  return {
    user_id: ctx.state.user._id,
    topic_id: ctx.request.body.topic_id
  };
}

async function changeBody(body, files) {
  if (body.pic) {
    const pic = files && files[body.pic] && files[body.pic].name;

    if (pic) {
      body.pic = pic;
    }
  } else if (files && files.pic) {
    body.pic = files.pic.name;
  }

  if (body.data) {
    for (let dataItem of body.data) {
      if (dataItem.type === config.dataTypes.IMAGE) {
        dataItem.data = files && files[dataItem.data] && files[dataItem.data].name;
      }

      if (dataItem.type === config.dataTypes.POLL) {
        dataItem.data.options = dataItem.data.options
          .map((o) => {
            if (o.pic) {
              o.pic = files && files[o.pic] && files[o.pic].name;
            }

            return o;
          });

        const poll = await Poll.create(dataItem.data);

        delete dataItem.data;
        dataItem._id = poll._id;
      }
    }
  }

  return body;
}

function sortByTime(a, b) {
  return new Date(b.updated_at).getTime() - new Date(a.updated_at).getTime();
}

module.exports = {
  index,
  explore,
  history,
  direct,
  favorite,
  show,
  create,
  update,
  destroy,
  topicFavorite,
  notInterested,
  like,
  openTopic,
  closeTopic
};

const CommentCtrl = require('../controllers/comment'); // cyclic dependencies in node js // todo fix?
