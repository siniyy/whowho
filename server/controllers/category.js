'use strict';

const config = require('config');
// TODO re-check with Pasha
const topicController = require('./topic');

const Category = require('./../models/category');
const User = require('./../models/user');
const initialData = require('./../libs/helpers/initialData');

async function index(ctx) {
  const categories = initialData.getCategories({main: ctx.query.main === 'true', sub: ctx.query.sub === 'true', title: ctx.query.search});

  const data = {
    other: categories
  };

  const includeTypes = ctx.query.include || '';

  if (includeTypes.includes('chosen')) {
    const user = await User.findById(ctx.state.user._id).lean();

    data.chosen = categories.filter(c => user.categories.find(uc => uc._id.equals(c._id)));
    data.other = categories.filter(c => !data.chosen.find(r => r._id === c._id));
  }

  if (includeTypes.includes('like')) {
    data.like = [];
  }

  if (includeTypes.includes('pop')) {
    data.pop = data.other.splice(0, config.popularCount);
  }

  ctx.data = data;
}

async function show(ctx) {
  ctx.data = initialData.getCategories({_id: ctx.params.id, sub: ctx.query.sub === 'true'});
}

async function create(ctx) {
  const category = await Category.create(changeCategoryBody(ctx.request.body, ctx.request.files));

  await initialData.setCategories();

  ctx.data = category;
}

async function update(ctx) {
  const data = changeCategoryBody(ctx.request.body, ctx.request.files);

  await Category.update({_id: ctx.params.id}, data);
  await initialData.setCategories();

  ctx.data = Object.assign({}, {_id: ctx.params.id}, data);
}

function changeCategoryBody(body, files) {
  if (body.pic) {
    const pic = files && files[body.pic] && files[body.pic].name;

    if (pic) {
      body.pic = pic;
    }
  } else if (files && files.pic) {
    body.pic = files.pic.name;
  }

  return body;
}

async function destroy(ctx) {
  const category = await Category.findById(ctx.params.id);

  for (let topic of category.topics) {
    await topicController.destroy({
      params: {
        id: topic._id,
      }
    });
  }

  await Category.remove({_id: ctx.params.id});

  await initialData.setCategories();
}

module.exports = {
  index,
  show,
  create,
  update,
  destroy
};