const path = require('path');

module.exports = {
  secret: 'e1i250lta78qbs47dnk3bgns93miblrq8',

  uri: 'http://localhost:8888',

  mongo: {
    uri: 'mongodb://localhost/whowho'
  },

  auth: {
    facebook: {
      clientID: '1908104719203949',
      clientSecret: '77d706f490883fba8d434f7f0cb90d97',
      callbackURL: 'http://whowho.local:8888/api/auth/facebook/callback',
      profileFields: ['email', 'displayName', 'photos', 'birthday', 'location'],
      url: 'https://graph.facebook.com/debug_token?input_token=%s&access_token=%s|%s'
    },

    google: {
      clientID: '987697904748-3ibgkg2v8dmcvkq7guvjsp0s566pcpd0.apps.googleusercontent.com',
      clientSecret: '6A8s0dzD0cMSIp9Nj4u01B2c',
      callbackURL: 'http://whowho.com:8888/api/auth/google/callback'
    }
  },

  uploadDir: './../uploads/',

  root: path.join(__dirname, '..'),

  targetedTime: 1000 * 60 * 60 * 24 * 2, // 2 days

  targetCount: 25,
  popularCount: 4,
  topicsOnPage: 20,
  commentsOnPage: 100,
  maxTopicShowCount: 30000000000, // set normal value in the future (3)

  imageSizes: {
    small: 400,
    medium: 800,
    large: 1200,
    original: 0 // should be in the end
  },

  dataTypes: {
    TEXT: 'text',
    IMAGE: 'img',
    POLL: 'poll'
  },

  hooks: {
    TOPIC_CREATE: 'TOPIC_CREATE',
    TOPIC_MODERATE: 'TOPIC_MODERATE',
    TOPIC_UPDATE: 'TOPIC_UPDATE',
    TOPIC_REMOVE: 'TOPIC_REMOVE',
    TOPIC_OPEN: 'TOPIC_OPEN',
    TOPIC_CLOSE: 'TOPIC_CLOSE',
    TOPIC_NOT_INTERESTED: 'TOPIC_NOT_INTERESTED',
    TOPIC_ADD_TO_DIRECT: 'TOPIC_ADD_TO_DIRECT',
    TOPIC_ADD_TO_FAVORITE: 'TOPIC_ADD_TO_FAVORITE',
    TOPIC_REMOVE_FROM_FAVORITE: 'TOPIC_REMOVE_FROM_FAVORITE',
    COMMENT_CREATE: 'COMMENT_CREATE',
    POLL_REMOVE: 'POLL_REMOVE',
    POLL_VOTE: 'POLL_VOTE',
    USER_REMOVE: 'USER_REMOVE'
  },

  socket: {
    CONNECTION: 'connection',
    DISCONNECT: 'disconnect',
    SUCCESS_CONNECTION: 'success_connection',
    OPEN_TOPIC: 'open_topic',
    CLOSE_TOPIC: 'close_topic',
    NEW_COMMENT: 'new_comment',
    NEW_COMMENT_NOTIFICATION: 'new_comment_notification',
    UPDATE_COMMENT: 'update_comment',
    REMOVE_COMMENT: 'remove_comment',
    COMMENT_LIKE: 'comment_like',
    GET_COMMENTS: 'get_comments',
    TOPICS_NOTIFICATIONS_OPEN: 'topics_notifications_open',
    TOPICS_NOTIFICATIONS_CLOSE: 'topics_notifications_close'
  },

  topicsForExplore: {
    topLikesHoursAgo: 5,
    topCommentsHoursAgo: 1,
    qtyToFetch: 500
  },

  apns: {
    host: 'api.development.push.apple.com',
    certificate: 'AuthKey_L736U52534.p8',
    keyId: 'L736U52534',
    teamId: 'J8P2E3B8U2',
    bundleId: 'com.whowho.messenger'
  }
};