module.exports = {
  secret: '6gbgcjegoiogfuu03jvvj86pp28e2v62o',

  auth: {
    facebook: {
      callbackURL: 'https://rocky-forest-20187.herokuapp.com/api/auth/facebook/callback'
    },
    google: {
      callbackURL: 'https://rocky-forest-20187.herokuapp.com/api/auth/google/callback'
    }
  },

  apns: {
    // host: 'api.push.apple.com' //todo set after release
  }
};