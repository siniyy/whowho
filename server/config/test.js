module.exports = {
  mongo: {
    uri: 'mongodb://localhost/whowho-test',
    options: {
      useMongoClient: true
    }
  },
};