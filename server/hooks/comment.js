'use strict';

const Notifications = require('../models/notification');

async function create(data) {
  await Notifications
    .update({
      topic_id: data.topic_id
    }, {
      $inc: {
        notifications_qty: 1
      }
    }, {
      multi: true
    });
}

module.exports = {
  create
};