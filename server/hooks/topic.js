'use strict';

const Topic = require('../models/topic');
const Category = require('../models/category');
const Comment = require('../models/comment');
const User = require('../models/user');
const Notifications = require('../models/notification');
const notifyModerators = require('../libs/notify_moderators');

const initialData = require('../libs/helpers/initialData');

async function addToCategories(topic_id, categoriesIds) {
  await Category.update({
    _id: {
      $in: categoriesIds
    }
  }, {
    $push: {
      topics: {
        _id: topic_id
      }
    },
    $inc: {
      posts_qty: 1,
      rating: 1
    }
  }, {
    multi: true
  });
}

async function removeFromCategories(topic_id, categoriesIds) {
  await Category.update({
    _id: {
      $in: categoriesIds
    }
  }, {
    $pull: {
      topics: {
        _id: topic_id
      }
    },
    $inc: {
      posts_qty: -1
    }
  }, {
    multi: true
  });
}

async function create(data) {
  await notifyModerators('topics', data);
}

async function moderate(data) {
  console.log('topic.js::moderate::56 >>>> ',1111111);
  const categoryBreadCrumb = initialData.getBreadCrumbs(data.category_id);

  if (categoryBreadCrumb) {
    const categoriesIds = categoryBreadCrumb.map(c => c._id);

    await addToCategories(data._id, categoriesIds);
  }

  await User
    .update({
      _id: {
        $in: data.users
      },
      'categories._id': data.category_id
    }, {
      $push: {
        topics: {
          _id: data._id,
          isTargeted: true
        }
      },
      $set: {
        'categories.$.targeted_at': new Date()
      }
    }, {
      multi: true
    });
}

async function update(data) {
  const topic = await Topic.findById(data.topic_id);

  const categoryBreadCrumb = initialData.getBreadCrumbs(topic.category_id);

  if (categoryBreadCrumb) {
    const categoriesIds = categoryBreadCrumb.map(c => c._id);

    // if category changed - need to move topic inside categories list (* else)
    if (!data.category_id || topic.category_id.equals(data.category_id)) {
      await Category
        .update({
          _id: {
            $in: categoriesIds
          },
          'topics._id': topic._id
        }, {
          $set: {
            'topics.$.updated_at': new Date()
          }
        }, {
          multi: true
        });
    } else {
      const oldCategoryBreadCrumb = initialData.getBreadCrumbs(data.category_id);

      if (oldCategoryBreadCrumb) {
        const oldCategoriesIds = oldCategoryBreadCrumb.map(c => c._id);
        await removeFromCategories(topic._id, oldCategoriesIds);
      }

      await addToCategories(topic._id, categoriesIds);
    }
  }

  await User
    .update({
      _id: {
        $in: topic.users
      },
      'topics._id': topic._id
    }, {
      $set: {
        'topics.$.updated_at': new Date()
      }
    }, {
      multi: true
    });

  const topicBody = {
    updated_at: new Date()
  };

  if (data.last_comment_id) {
    topicBody.last_comment_id = data.last_comment_id;
  }

  await Topic
    .update({
      _id: topic._id
    }, topicBody);
}

async function destroy(data) {
  const topic = await Topic.findById(data.topic_id).lean();

  await Notifications.remove({
    topic_id: topic._id
  });

  await Comment.remove({
    topic_id: topic._id
  });


  const categoryBreadCrumb = initialData.getBreadCrumbs(topic.category_id);

  if (categoryBreadCrumb) {
    const categoriesIds = categoryBreadCrumb.map(c => c._id);

    await removeFromCategories(topic._id, categoriesIds);
  }

  await User
    .update({
      _id: {
        $in: topic.users
      }
    }, {
      $pull: {
        topics: {
          _id: topic._id
        }
      }
    });
}

async function open(data) {
  await Topic
    .update({
      _id: data.topic_id
    }, {
      $addToSet: {
        active_users: data.user_id
      }
    });

  await changeUserTopicsListByType(data, 'isHistory');

  // result => {notifications_qty, comment_id}
  const notifications = await Notifications
    .findOne({
      user_id: data.user_id,
      topic_id: data.topic_id
    }, '-_id -__v')
    .lean()
    .then((res) => {
      if (res) {
        return res;
      }

      return Notifications
        .create({user_id: data.user_id, topic_id: data.topic_id})
        .then(() => {
          return {
            notifications_qty: null,
            comment_id: null,
            first_open: true
          }
        });
    });

  return Object.assign({}, notifications);
}

async function close(data) {
  await Notifications
    .update({
      topic_id: data.topic_id,
      user_id: data.user_id
    }, {
      $set: {
        comment_id: data.comment_id || null,
        notifications_qty: data.notifications_qty || 0
      }
    });

  await Topic
    .update({
      _id: data.topic_id
    }, {
      $pull: {
        active_users: data.user_id
      }
    });
}

async function notInterested(data) {
  await User
    .update({
      _id: data.user_id
    }, {
      $pull: {
        topics: {
          _id: data.topic_id
        }
      },
      $addToSet: {
        notInterested: data.topic_id
      }
    });

  await Topic
    .update({
      _id: data.topic_id
    }, {
      $pull: {
        users: data.user_id,
        active_users: data.user_id
      }
    });
}

async function addToDirect(data) {
  await changeUserTopicsListByType(data, 'isDirect');
}

async function addToFavorite(data) {
  await changeUserTopicsListByType(data, 'isFavorite');
}

async function removeFromFavorite(data) {
  await changeUserTopicsListByType(data, 'isFavorite', false);
}

async function changeUserTopicsListByType(data, type, value = true) {
  const res = await User
    .update({
      _id: data.user_id,
      'topics._id': data.topic_id
    }, {
      $set: {
        [`topics.$.${type}`]: value
      }
    });

  if (!res.n) { // if not exist in user topics array
    await User
      .update({
        _id: data.user_id
      }, {
        $push: {
          topics: {
            _id: data.topic_id,
            [type]: value
          }
        }
      });
  }
}

module.exports = {
  create,
  moderate,
  update,
  destroy,
  open,
  close,
  notInterested,
  addToDirect,
  addToFavorite,
  removeFromFavorite
};