'use strict';

const Topic = require('../models/topic');

async function destroy(user) {
  const topicsIds = user.topics.map(t => t._id);

  await Topic
    .update({
      _id: {
        $in: topicsIds
      }
    }, {
      $pull: {
        users: user._id,
        active_users: user._id
      }
    }, {
      multi: true
    });
}

module.exports = {
  destroy
};