'use strict';

const UserPoll = require('../models/user_poll');

async function destroy(_id) {
  await UserPoll.remove({_id});
}

async function vote({poll_id, user_id, option_id}) {
  await UserPoll
    .create({
      poll_id,
      user_id,
      option_id
    });
}

module.exports = {
  destroy,
  vote
};