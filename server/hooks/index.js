'use strict';

const config = require('config');

const topic = require('./topic');
const user = require('./user');
const poll = require('./poll');
const comment = require('./comment');

function doHook(hook, data) {
  switch(hook) {
    case config.hooks.TOPIC_CREATE:
      return topic.create(data);

    case config.hooks.TOPIC_MODERATE:
      return topic.moderate(data);

    case config.hooks.TOPIC_UPDATE:
      return topic.update(data);

    case config.hooks.TOPIC_REMOVE:
      return topic.destroy(data);

    case config.hooks.TOPIC_OPEN:
      return topic.open(data);

    case config.hooks.TOPIC_CLOSE:
      return topic.close(data);

    case config.hooks.TOPIC_NOT_INTERESTED:
      return topic.notInterested(data);

    case config.hooks.TOPIC_ADD_TO_DIRECT:
      return topic.addToDirect(data);

    case config.hooks.TOPIC_ADD_TO_FAVORITE:
      return topic.addToFavorite(data);

    case config.hooks.TOPIC_REMOVE_FROM_FAVORITE:
      return topic.removeFromFavorite(data);

    case config.hooks.COMMENT_CREATE:
      return comment.create(data);

    case config.hooks.USER_REMOVE:
      return user.destroy(data);

    case config.hooks.POLL_REMOVE:
      return poll.destroy(data);

    case config.hooks.POLL_VOTE:
      return poll.vote(data);

    default:
      return;
  }
}

module.exports = doHook;