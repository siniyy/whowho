'use strict';

const Topic = require('../models/topic');
const Category = require('../models/category');
const User = require('../models/user');
const Notification = require('../models/notification');
const sinon = require('sinon');
const chai = require('chai');
const should = chai.should();
const chaiAsPromised = require('chai-as-promised');
const categoryData = require('./fixtures/topic/category');
const topicData = require('./fixtures/topic/topic');
const {create} = require('../controllers/topic');
const USERS_TO_ADD = 13;

chai.use(chaiAsPromised);

describe('TOPIC', async () => {
  let topic, category;

  before(async () => {
    const user = await User.findOne({name: 'test_user_1'});
    category = await Category.create(categoryData);

    const {_id} = await create({
      request: {
        body: Object.assign(topicData, {
          category_id: category._id,
          user_id: user._id,
          usersToAdd: USERS_TO_ADD
        })
      },
      state: {
        user
      }
    });

    topic = await Topic.findById(_id);
  });

  describe('Create', () => {
    before(async () => {
      category = await Category.findById(category._id); // updated category data
    });

    it('should exist with all required fields', () => {
      topic.should.exist;
      topic.title.should.be.a('string').and.not.to.be.empty;
      topic.category_id.should.exist;
      topic.user_id.should.exist;
    });

//    it('should create topic poll', () => {});

    it('should be added to category', () => {
      // FIXME category.topics => [{topic_id: null, updated_at: null}]
      category.topics.should.be.an('array').that.includes({topic_id: topic._id});
    });

    it('should increase category rating', () => {
      category.rating.should.equal(3);
    });

    it('should increase category posts quantity', () => {
      category.posts_qty.should.equal(1);
    });

    it(`should contain ${USERS_TO_ADD} users`, () => {
      topic.users.should.be.an('array').that.has.lengthOf(USERS_TO_ADD);
    });

    it('should be added to chosen users in "users.topics"', async () => {
      await topic
        .populate('users')
        .execPopulate()
        .then((users) => {
          users.map((user) => {
            user.topics.should.include({topicId: topic._id}); // FIXME maybe ObjectId(topic._id)
          });
        });
    });

    it('should be added with chosen users to "notifications"', async () => {
      const userIds = await topic
        .populate('users')
        .execPopulate()
        .then(users => user.map(user => user._id));

      const notifications = await Notification.find({
        user_id: {
          $in: userIds
        },
        topic_id: topic._id
      });

      notifications.should.be.an('array').that.has.lengthOf(USERS_TO_ADD);
    });

    it('should send push on UI', () => {
      // sinon.mock...
    });
  });

  describe('Open', () => {
    let users;

    before(async () => {
      // await topicActions.open();
    });

    it('should reset notifications quantity for current users', async () => {
      await Notification
        .find({
          user_id: {
            $in: users.map(user => user.id)
          },
          topic_id: topic._id
        })
    });

    it('should have 2 active users', () => {
      topic.active_users.should.be.an('array').that.has.lengthOf(2);
    });
  });

  describe('Close', () => {
    before(async () => {
      // await topicActions.close();
    });

    it('should set notifications last_viewed to now() for current users', () => {});

    it('should have 0 active users', () => {
      topic.active_users.should.be.an('array').that.is.empty;
    });
  });

  describe('Remove', () => {
    before(async () => {
      // await topicActions.remove();
    });

    it('should be removed with chosen users from "notifications"', () => {});
    it('should decrement category post quantity', () => {});
    it('should be removed to chosen users in "users.topics"', () => {});
    it('should be removed from category', () => {});
    it('should remove all topic comments', () => {});
//    it('should remove topic poll', () => {});
  });

  after(async () => {
    await Topic.remove({});
    await Category.remove({});
  })
});