const path = require('path');

process.env.NODE_CONFIG_DIR = path.join(__dirname, '../config');
process.env.NODE_ENV = 'test';

const mongoose = require('../libs/mongoose');
const User = require('../models/user');

const usersData = require('./fixtures/users');

before((done) => {
  mongoose.connection.on('open', async () => {
    mongoose.connection.db.dropDatabase(async () => {
      await User.create(usersData);
      done();
    });
  });
});

after(() => process.exit()); // while testing
//after(() => mongoose.disconnect());