'use strict';

const chai = require('chai');
const should = chai.should();
const Category = require('../models/category');
const categoryData = require('./fixtures/topic/category');
const {create} = require('../controllers/category');


describe('CATEGORY', () => {
  let category;

  before(async () => {
    category = await Category.create(categoryData);
  });

  describe('Create', () => {
    it('should create category with all required fields', () => {
      category.should.exist;
      category.title.should.be.a('string').and.not.to.be.empty;
      category.topics.should.be.an('array').that.is.empty;
      category.posts_qty.should.equal(0);
      category.rating.should.equal(0);
    });

    it('should throw error for duplicate title', () => {
      return Category
        .create(categoryData)
        .catch(err => err.code.should.equal(11000)); // MongoError: duplicate key
    });

    it('should create child', async () => {
      const parentCategory = await Category
        .create({
          title: 'test_category_child',
          category_id: category._id
        }) // create child
        .then(res => res.populate('category_id').execPopulate()) // populate parent
        .then(res => res.category_id); // return parent

      parentCategory._doc.should.eql(category._doc);
    });
  });

  describe('Remove', () => {
    // TODO after category_actions.remove will be implemented
  });

  after(async () => {
    await Category.remove({});
  })
});