'use strict';

const config = require('config');
const hooks = require('../hooks');
const initialData = require('../libs/helpers/initialData');

const resources = {
  topics: {
    name: 'topic',
    model: require('../models/topic'),
    moderateHook: config.hooks.TOPIC_MODERATE,
  },
  categories: {
    name: 'category',
    model: require('../models/category'),
    // moderateHook: config.hooks.CATEGORY_MODERATE,
  },
  users: {
    name: 'user',
    model: require('../models/user'),
  },
};

const updateModetors = async (resource) => {
  if (resource === 'user') {
    await initialData.setModerators();
  }
};

async function get(ctx) {
  ctx.data = await resources[ctx.params.type].model.find();
}

async function put(ctx) {
  const resource = resources[ctx.params.type].name;
  const resourceController = require(`../controllers/${resource}`);

  await resourceController.update.call(this, ctx);

  await updateModetors(resource);
}

async function post(ctx) {
  const resource = resources[ctx.params.type].name;
  const resourceController = require(`../controllers/${resource}`);

  if (ctx.request.body) {
    ctx.request.body.fromAdmin = true;
  }

  await resourceController.create.call(this, ctx);

  await updateModetors(resource);
}

async function remove(ctx) {
  const resource = resources[ctx.params.type].name;
  const resourceController = require(`../controllers/${resource}`);

  await resourceController.destroy.call(this, ctx);

  await updateModetors(resource);
}

async function moderate(ctx) {
  const resource = resources[ctx.params.type];

  await resource.model.update(
    { _id: ctx.request.body.id },
    { moderated: true },
  );

  const data = await resource.model.findById(ctx.request.body.id);

  await hooks(resource.moderateHook, data);

  // TODO send push
  // users.forEach((user) => {
  //
  // });
}

module.exports = {
  get,
  put,
  post,
  remove,
  moderate,
};