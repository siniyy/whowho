const Router = require('koa-router');
const router = new Router();

const adminController = require('./controller');

router.get('/:type', adminController.get);
router.put('/:type/:id', adminController.put);
router.post('/:type', adminController.post);
router.delete('/:type/:id', adminController.remove);

router.post('/:type/moderate', adminController.moderate);

module.exports = router.routes();