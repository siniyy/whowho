'use strict';

const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../config');

require('../libs/mongoose');

const moment = require('moment');
const _ = require('lodash');
const config = require('config').topicsForExplore;
const logger = require('../libs/logger')(module);
const Category = require('../models/category');
const Topic = require('../models/topic');
const Comment = require('../models/comment');

async function run() {
  console.log('staring');

  const categories = await Category.find({category_id: null}, 'title topics newest popular');

  let byComments = [];
  let byLikes = [];
  let newest = [];
  let category;
  let ids;

  const limit = Math.ceil(config.qtyToFetch / 2); // half for comments, half for likes

  for (category of categories) {
    logger.info(`Category "${category.title}" (${category._id}): started`);

    // top likes
    const topLikesHoursAgo = moment().subtract(config.topLikesHoursAgo, 'hour').toDate();

    ids = category.topics
      .filter(topic => new Date(topic.updated_at) >= topLikesHoursAgo)
      .map(t => t._id);

    byLikes = await Topic
      .find({
        _id: {
          $in: ids
        }
      })
      .sort({likes_qty: -1})
      .limit(limit)
      .then(topics => topics.map(t => t._id));



    // top comments
    const topCommentsHoursAgo = moment().subtract(config.topCommentsHoursAgo, 'hours').toDate();

    ids = category.topics
      .filter(topic => new Date(topic.updated_at) >= topCommentsHoursAgo)
      .map(i => i._id);

    byComments = await Comment.aggregate([
      {
        $match: {
          created_at: {$gt: topCommentsHoursAgo},
          topic_id: {$in: ids}
        }
      },
      {
        $group: {
          _id: '$topic_id',
          count: {$sum: 1}
        }
      },
      {
        $sort: {count: -1}
      },
      {
        $limit: limit
      }
    ]);

    byComments = _(byComments)
      .map(t => t._id)
      .unionWith(byLikes, category.popular, _.isEqual)
      .slice(0, config.qtyToFetch)
      .shuffle()
      .value();


    // newest
    newest = _(category.topics)
      .sort((t1, t2) => new Date(t1.created_at) - new Date(t2.created_at))
      .slice(0, config.qtyToFetch)
      .shuffle()
      .value();

    // update category
    await category.update({
      newest,
      popular: byComments
    });

    logger.info(`Category "${category.title}" (${category._id}): updated\n`);
  }
}

run().catch(err => (logger.error(err), -1)).then(process.exit);