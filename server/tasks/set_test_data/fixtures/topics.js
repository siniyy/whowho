'use strict';

const Topic = require('../../../models/topic');
const oid = require('../../../libs/oid');
const moment = require('moment');

const data = [{
  _id: oid('topic_1'),
  title: 'topic_1',
  pic: 'topic-test.png',
  data: [
    {
      type: 'text',
      data: 'dsdsdsds,'
    },
    {
      type: 'text',
      data: 'sddsdsds,'
    },
    {
      type: 'img',
      data: 'body.jpg'
    }
  ],
  likes: [oid('user_1'),oid('user_2'),oid('user_3')],
  likes_qty: 3,
  category_id: oid('category_1'),
  user_id: oid('user_5')
},{
  _id: oid('topic_2'),
  title: 'topic_2',
  category_id: oid('category_1'),
  likes: [oid('user_1')],
  likes_qty: 1,
  user_id: oid('user_5')
},{
  _id: oid('topic_3'),
  title: 'topic_3',
  likes: [oid('user_1'),oid('user_2'),oid('user_3'),oid('user_4')],
  likes_qty: 4,
  category_id: oid('category_2'),
  user_id: oid('user_5')
},{
  _id: oid('topic_4'),
  title: 'topic_4',
  category_id: oid('category_2'),
  user_id: oid('user_5')
},{
  _id: oid('topic_5'),
  title: 'topic_5',
  category_id: oid('category_4'),
  user_id: oid('user_1')
},{
  _id: oid('topic_6'),
  title: 'topic_6',
  category_id: oid('category_3'),
  user_id: oid('user_2')
},{
  _id: oid('topic_7'),
  title: 'topic_7',
  category_id: oid('category_1'),
  likes: [oid('user_1'),oid('user_2')],
  likes_qty: 2,
  user_id: oid('user_5')
},{
  _id: oid('topic_8'),
  title: 'topic_8',
  likes: [oid('user_1'),oid('user_2'),oid('user_4')],
  likes_qty: 3,
  category_id: oid('category_4'),
  user_id: oid('user_5')
}];

async function load() {
  await Topic.insertMany(data);
}

function getDate(hoursToSubtract = 0) {
  return moment().subtract(hoursToSubtract, 'hours').toDate();
}

module.exports = load;