'use strict';

const Category = require('../../../models/category');
const oid = require('../../../libs/oid');
const moment = require('moment');

const data = [{
  _id: oid('category_1'),
  title: 'category_1',
  pic: 'test.jpg',
  topics: [{
    _id: oid('topic_1'),
    updated_at: getDate(),
    created_at: getDate(30)
  },{
    _id: oid('topic_2'),
    updated_at: getDate(),
    created_at: getDate(111)
  },{
    _id: oid('topic_7'),
    updated_at: getDate(),
    created_at: getDate(4)
  }]
},{
  _id: oid('category_2'),
  title: 'category_2',
  topics: [{
    _id: oid('topic_3'),
    updated_at: getDate()
  },{
    _id: oid('topic_4'),
    updated_at: getDate()
  }]
},{
  _id: oid('category_3'),
  title: 'category_3',
  topics: [{
    _id: oid('topic_6'),
    updated_at: getDate()
  }]
},{
  _id: oid('category_4'),
  title: 'category_4',
  topics: [{
    _id: oid('topic_5'),
    updated_at: getDate()
  },{
    _id: oid('topic_8'),
    updated_at: getDate()
  }]
}];



async function load() {
  await Category.insertMany(data);
}

function getDate(hoursToSubtract = 0) {
  return moment().subtract(hoursToSubtract, 'hours').toDate();
}

module.exports = load;