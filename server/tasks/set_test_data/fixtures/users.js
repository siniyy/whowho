'use strict';

const User = require('../../../models/user');
const oid = require('../../../libs/oid');
const moment = require('moment');

const data = [{
  _id: oid('admin'),
  email: 'test',
  name: 'admin',
  is_admin: true,
},{
  _id: oid('user_1'),
  name: 'user_1',
  email: 'email_1',
  categories: [{_id: oid('category_1')}, {_id: oid('category_2')}, {_id: oid('category_3')}],
  topics: [{
    _id: oid('topic_1'),
    updated_at: getDate()
  },{
    _id: oid('topic_2'),
    updated_at: getDate()
  },{
    _id: oid('topic_3'),
    updated_at: getDate()
  }]
},{
  _id: oid('user_2'),
  name: 'user_2',
  email: 'email_2',
  categories: [{_id: oid('category_4')}],
  topics: [{
    _id: oid('topic_8'),
    updated_at: getDate()
  },{
    _id: oid('topic_5'),
    updated_at: getDate()
  }]
},{
  _id: oid('user_3'),
  name: 'user_3',
  email: 'email_3',
  categories: [{_id: oid('category_2')}, {_id: oid('category_4')}],
  topics: [{
    _id: oid('topic_4'),
    updated_at: getDate()
  },{
    _id: oid('topic_5'),
    updated_at: getDate()
  }]
},{
  _id: oid('user_4'),
  name: 'user_4',
  email: 'email_4',
  categories: [{_id: oid('category_1')}, {_id: oid('category_3')}],
  topics: [{
    _id: oid('topic_6'),
    updated_at: getDate()
  },{
    _id: oid('topic_7'),
    updated_at: getDate()
  }]
},{
  _id: oid('user_5'),
  name: 'user_5',
  email: 'email_5',
  categories: [{_id: oid('category_1')}, {_id: oid('category_2')}, {_id: oid('category_3')}, {_id: oid('category_4')}]
}];



async function load() {
  await User.insertMany(data);
}

function getDate(hoursToSubtract = 0) {
  return moment().subtract(hoursToSubtract, 'hours').toDate();
}

module.exports = load;