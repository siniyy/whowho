'use strict';

const Comment = require('../../../models/comment');
const oid = require('../../../libs/oid');
const moment = require('moment');

const data = [{
  _id: oid('comment_1'),
  text: 'comment_1',
  likes: [oid('user_1'),oid('user_2'),oid('user_3')],
  topic_id: oid('topic_1'),
  user_id: oid('user_4')
},{
  _id: oid('comment_2'),
  text: 'comment_2',
  likes: [oid('user_1')],
  topic_id: oid('topic_1'),
  user_id: oid('user_2')
},{
  _id: oid('comment_3'),
  text: 'comment_3',
  topic_id: oid('topic_1'),
  user_id: oid('user_3')
},{
  _id: oid('comment_4'),
  text: 'comment_4',
  topic_id: oid('topic_1'),
  user_id: oid('user_2'),
  comment_id: oid('user_2')
},{
  _id: oid('comment_5'),
  text: 'comment_5',
  likes: [oid('user_2')],
  topic_id: oid('topic_1'),
  user_id: oid('user_4')
},{
  _id: oid('comment_6'),
  text: 'comment_6',
  topic_id: oid('topic_3'),
  user_id: oid('user_1')
},{
  _id: oid('comment_7'),
  text: 'comment_7',
  topic_id: oid('topic_3'),
  user_id: oid('user_2')
}];

[...Array(1000)].forEach((v, i) => data.push({
  _id: oid(`comment_${i+8}`),
  text: `comment_${i+8}`,
  topic_id: oid('topic_2'),
  user_id: oid('user_2'),
  created_at: getDate(1000 - i)
}));

async function load() {
  await Comment.insertMany(data);
}

function getDate(hoursToSubtract = 0) {
  return moment().subtract(hoursToSubtract, 'hours').toDate();
}

module.exports = load;