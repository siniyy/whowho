'use strict';

const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../config');

const mongoose = require('../../libs/mongoose');
const logger = require('../../libs/logger')(module);

const fs = require('fs');

async function run() {
  await mongoose.connection.on('open', async () => {
    mongoose.connection.db.dropDatabase();
  });

  const files = fs.readdirSync(`${__dirname}/fixtures`).sort();

  for (let fileName of files) {
    await require(`./fixtures/${fileName}`)();
  }
}

run().catch(err => (logger.error(err), -1)).then(process.exit);