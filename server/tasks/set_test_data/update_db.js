'use strict';

const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../config');

const mongoose = require('../../libs/mongoose');
const logger = require('../../libs/logger')(module);

const User = require('../../models/user');

async function run() {
  const users = await User.find().lean().then(res => {
    return res.map(b => {
      b.categories = b.categories.map(c => {
        return {
          _id: c._id
        }
      });

      return b;
    });
  });

  await User.remove();

  await User.create(users);
}

run().catch(err => (logger.error(err), -1)).then(process.exit);