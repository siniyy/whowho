const fs = require('fs');
const path = require('path');
const Router = require('koa-router');
const router = new Router();

router.get('/', (ctx) => {
  const filepath = path.join(__dirname, '../../frontend/dist/index.html');

  ctx.type = 'text/html';
  ctx.body = fs.createReadStream(filepath);
});

module.exports = router.routes();
