'use strict';

const category = require('../../controllers/category');

function init(router) {
  router.resources('categories', category);
}

module.exports = init;