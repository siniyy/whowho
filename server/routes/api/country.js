'use strict';

const country = require('../../controllers/country');

function init(router) {
  router.get('/country', country.index);
}

module.exports = init;