'use strict';

const Router = require('hello-resource-router');
const router = new Router();

// user
require('./user')(router);

// category
require('./category')(router);

// topic
require('./topic')(router);

// polls
require('./poll')(router);

// country
require('./country')(router);

// FIXME remove, used only for tests
// comments
require('./comment')(router);

module.exports = router.routes();