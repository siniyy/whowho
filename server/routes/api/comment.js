'use strict';

const comment = require('../../controllers/comment');

function init(router) {
  router.post('/comments', comment.createRest);
}

module.exports = init;