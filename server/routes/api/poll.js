'use strict';

const polls = require('../../controllers/poll');

function init(router) {
  router.post('/polls/vote', polls.vote);
}

module.exports = init;