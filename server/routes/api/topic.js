'use strict';

const topic = require('../../controllers/topic');

function init(router) {
  router.get('/topics/explore', topic.explore);
  router.get('/topics/history', topic.history);
  router.get('/topics/direct', topic.direct);
  router.get('/topics/favorite', topic.favorite);

  router.post('/topics/favorite', topic.topicFavorite);
  router.post('/topics/notInterested', topic.notInterested);
  router.post('/topics/like', topic.like);

  router.resources('topics', topic);
}

module.exports = init;