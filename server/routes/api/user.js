'use strict';

const user = require('../../controllers/user');

function init(router) {
  router.resources('users', user);

  router.post('/users/appleDeviceId', user.addAppleDeviceId);
  router.post('/users/logout', user.logout);
}

module.exports = init;