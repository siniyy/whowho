'use strict';

const config = require('config');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const Router = require('koa-router');
const router = new Router();

// social
router.get('/facebook/callback', passport.authenticate('facebook-token', {session: false}), retrieveUser);
router.get('/google/callback', passport.authenticate('google-token', {session: false}), retrieveUser);

// admin
router.post('/login', passport.authenticate('local', {session: false}), retrieveUser);

function retrieveUser(ctx) {
  const user = ctx.state.user;

  ctx.data = {
    _id: user._id,
    name: user.name,
    email: user.email,
    gender: user.gender,
    location: user.location,
    birthday: user.birthday,
    pic: user.pic,
  };

  ctx.set('token', generateToken({
    _id: user._id,
    isAdmin: user.is_admin,
  }));
}

function generateToken(data) {
  return jwt.sign(data, config.secret);
//  return jwt.sign({_id: user._id}, config.secret, {expiresIn: '1h'}); // FIXME discuss expires
}

module.exports = router.routes();