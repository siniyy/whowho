'use strict';

const Router = require('koa-router');
const hasAccess = require('../middlewares/access');

const userGuard = require('../middlewares/auth/user');
const adminGuard = require('../middlewares/auth/admin');

const router = new Router();

module.exports = function(app) {
  router.use(require('./static'));
  router.use('/api/auth', require('./auth'));

  // user access level
  router.use(userGuard);
  // FIXME move hasAccess to user guard
  router.use('/api', hasAccess, require('./api'));

  // admin access level
  router.use(adminGuard);
  router.use('/api/admin', require('../admin/routes'));

  router.all('*', (ctx) => ctx.throw(404));

  app
    .use(router.routes())
    .use(router.allowedMethods());
};
