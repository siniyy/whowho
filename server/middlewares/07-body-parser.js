'use strict';

const bodyParser = require('koa-body');
const config = require('config');
const path = require('path');
const crypto = require('crypto');

module.exports = bodyParser({
  multipart: true,
  formidable: {
    uploadDir: path.join(config.root, config.uploadDir),
    onFileBegin: (name, file) => {
      const fileName = `${crypto.randomBytes(20).toString('hex')}.${file.name.split('.').pop()}`;
      file.name = fileName;
      file.path = path.join(config.root, `${config.uploadDir}${fileName}`);
    }
  }
});