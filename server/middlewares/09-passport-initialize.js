'use strict';

const config = require('config');
const passport = require('koa-passport');
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook-token');
const GoogleStrategy = require('passport-google-token').Strategy;
const User = require('../models/user');

passport.use(new FacebookStrategy(config.auth.facebook, FacebookLogin));
passport.use(new GoogleStrategy(config.auth.google, GoogleLogin));

passport.use(new LocalStrategy({
  usernameField: 'email',
}, function(email, password, done) {
    User.findOne({ email }, function (err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done({ message: 'Invalid username or password' }, false);
      }

      // TODO check password

      return done(null, user);
    });
  }
));

function FacebookLogin(token, tokenSecret, profile, done) {
  const email = profile.emails && profile.emails[0].value ? profile.emails[0].value : `${profile.id}@fb.com`;
  const pic = profile.photos && profile.photos[0].value ? profile.photos[0].value : '';

  User.findOrCreate(
    {email},
    {
      email,
      facebook_id: profile.id,
      name: profile.displayName,
      pic,
      gender: profile.gender || '', // ???
      location: profile._json.location ? profile._json.location.name : '',
      birthday: profile._json.birthday || '',
      password: getPassword()
    },
    (err, user) => done(err, user)
  );
}

function GoogleLogin(accessToken, refreshToken, profile, done) { // TODO merge with facebook func
  const email = profile.emails ? profile.emails[0].value : `${profile.id}@ggl.com`;
  const pic = profile.photos ? profile.photos[0].value : '';

  User.findOrCreate(
    {
      email
    },
    {
      email,
      google_id: profile.id,
      gender: profile.gender || '',
      location: profile.location || '', // ???
      birthday: profile.birthday || '', // ???
      name: profile.displayName,
      pic,
      password: getPassword()
    },
    (err, user) => done(err, user)
  );
}

function getPassword() {
  return Math.random().toString(32).slice(2);
}

module.exports = passport.initialize();
