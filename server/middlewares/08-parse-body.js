'use strict';

const config = require('config');
const path = require('path');
const fs = require('mz/fs');
const jimp = require('jimp');

module.exports = async function (ctx, next) {
  if (ctx.request.body.files) {
    const fields = ctx.request.body.fields;
    const files = ctx.request.body.files;

    ctx.request.files = files;

    ctx.request.body = Object
      .keys(fields)
      .reduce((body, property) => {
        try {
          body[property] = JSON.parse(fields[property]);
        } catch (e) {
          body[property] = fields[property];
        }

        return body;
      }, {});
  }

  await next();

  if (ctx.request.files) {
    const parts = ctx.originalUrl.replace('/api/', '').split('/');
    let entity = parts[0] !== 'admin' ? parts[0] : parts[1];

    const mainDir = path.join(config.root, config.uploadDir);
    const folderDir = path.join(mainDir, entity);
    const entityDir = path.join(folderDir, ctx.data._id.toString());

    if (!await fs.exists(folderDir)) {
      await fs.mkdir(folderDir);
    }

    if (!await fs.exists(entityDir)) {
      await fs.mkdir(entityDir);

      for (let folder of Object.keys(config.imageSizes)) {
        await fs.mkdir(`${entityDir}/${folder}`);
      }
    }

    for (let fileName of Object.keys(ctx.request.files)) {
      const file = ctx.request.files[fileName];

      for (let folder of Object.keys(config.imageSizes)) {
        const newFile = `${entityDir}/${folder}/${file.name}`;

        await new Promise((resolve) => {
          if (config.imageSizes[folder]) {
            jimp
              .read(file.path, (err, image) => {
                try {
                  image.scaleToFit(config.imageSizes[folder], config.imageSizes[folder]).write(newFile, resolve);
                } catch (e) {
                  console.log(`JIMP SCALE ERROR: ${e.message}`);
                  resolve();
                }
              })
              .catch((e) => {
                console.log(`JIMP READ ERROR: ${e.message}`);
                resolve();
              });
          } else {
            fs.rename(file.path, newFile, resolve);
          }
        });
      }
    }
  }
};