const methodsToCheck = ['PUT', 'DELETE'];

async function checkAccess(resource, resource_id, user_id) {
  const mapping = {
    category: 'category',
    topics: 'topic',
    comments: 'comments',
    users: 'user'
  };

  const model = require(`../../models/${mapping[resource]}`);

  const query = {
    _id: resource_id
  };

  switch(resource) {
    case 'category':
    case 'topics':
    case 'comments':
      query.user_id = user_id;
      break;

    default: // user
      query._id = user_id;
  }

  return model.findOne(query);
}

async function hasAccess(ctx, next) {
  const [,, resource, resource_id] = ctx.originalUrl.split('/');

  if (methodsToCheck.includes(ctx.request.method)) {
    const user = ctx.state.user;

    if (!user.isAdmin) { // not implemented for now
      const isAllowedToProceed = await checkAccess(resource, resource_id, user._id);

      if (!isAllowedToProceed) {
        ctx.throw(403);
      }
    }
  }

  await next();
}

module.exports = hasAccess;