'use strict';

async function errorHandler(ctx, next) {
  try {
    await next();

    if (ctx.type !== 'text/html') {
      ctx.body = {
        success: true,
        data: ctx.data
      };
    }
  } catch (err) {
    ctx.status = err.statusCode || err.status || 500;

    if (err.stack) {
      console.error(err.message, err.stack);
    }

    if (err.code === 11000) {
      err.message = 'Duplicate entry';
    }

    ctx.body = {
      success: false,
      error: err.message || 'Internal error'
    };
  }
}

module.exports = errorHandler;
