'use strict';

const fs = require('fs');
const filesToSkip = ['index.js', 'access', 'auth'];

function include(app) {
  fs
    .readdirSync(__dirname)
    .sort()
    .forEach((middleware) => {
      if (filesToSkip.includes(middleware)) {
        return;
      }

      app.use(require(`./${middleware}`));
    });
}

module.exports = include;
