'use strict';

const socket = require('./../libs/socket');

async function socketHandler(ctx, next) {
  await next();

  if (ctx.socketData && socket[ctx.socketData.method]) {
    socket[ctx.socketData.method](...ctx.socketData.data);
  }
}

module.exports = socketHandler;
