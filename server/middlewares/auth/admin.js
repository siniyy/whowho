async function isAdmin(ctx, next) {
  if (ctx.state.user.isAdmin) {
    await next();
  } else {
    ctx.throw(401, 'Authentication Error');
  }
}

module.exports = isAdmin;