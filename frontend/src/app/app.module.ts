import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';

import { noop } from 'rxjs/index';

import { ActionsHandler } from '@app/shared/services/actions.handler';
import { TokenInterceptor } from '@app/shared/services/token.interceptor';

import { ComponentsModule } from '@app/shared/components';
import { MaterialModule } from '@app/shared/material';

import { AuthModule } from '@app/auth/auth.module';

import { environment } from '@env/environment';
import { routes } from '@app/app.routes';

import { AppComponent } from '@app/app.component';
import { NotFoundPageComponent } from '@app/not-found-page/not-found-page.component';
import { AppState } from '@app/shared/store/app.state';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundPageComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MaterialModule,
    ComponentsModule,

    RouterModule.forRoot(routes, { useHash: true }),

    NgxsModule.forRoot([AppState]),
    NgxsRouterPluginModule.forRoot(),
    NgxsFormPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production }),
    NgxsLoggerPluginModule.forRoot({
      disabled: environment.production,
      logger: console,
      collapsed: true,
    }),
    NgxsStoragePluginModule.forRoot({
      key: ['admin'],
    }),

    AuthModule.forRoot(),
  ],
  providers: [
    ActionsHandler,
    {
      provide: APP_INITIALIZER,
      useFactory: () => noop,
      deps: [ActionsHandler],
      multi: true,
    },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
