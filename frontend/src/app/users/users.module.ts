import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { MaterialModule } from '@app/shared/material';
import { ComponentsModule } from '@app/shared/components';

import { UserListPageComponent } from '@app/users/containers/user-list-page/user-list-page.component';
import { UserEditPageComponent } from '@app/users/containers/user-edit-page/user-edit-page.component';
import { UserFormComponent } from '@app/users/components/user-form-component/user-form.component';

import { UserState } from '@app/users/store/user.state';
import { UsersActivator } from '@app/users/services/users.activator';
import { UserService } from '@app/users/services/user.service';

import { userRoutes } from '@app/users/users.routes';

export const COMPONENTS = [
  UserListPageComponent,
  UserEditPageComponent,
  UserFormComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    ComponentsModule,
    RouterModule.forChild(userRoutes),

    NgxsFormPluginModule,
    NgxsModule.forFeature([UserState]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [
    UsersActivator,
    UserService,
  ],
})
export class UsersModule {
}