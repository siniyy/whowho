import { User } from '@app/users/models/user.model';

export class LoadUsers {
  static readonly type = '[Users] Load';
}

export class LoadUsersSuccesss {
  static readonly type = '[Users] Load Success';
  constructor(public payload: User[]) {}
}

export class LoadUsersFailure {
  static readonly type = '[Users] Load Failure';
  constructor(public payload: any) {}
}

export class SaveUser {
  static readonly type = '[Users] Save';
  constructor(public payload: User) {}
}

export class SaveUserSuccess {
  static readonly type = '[Users] Save Success';
  constructor(public payload: User) {}
}

export class SaveUserFailure {
  static readonly type = '[Users] Save Failure';
  constructor(public payload: any) {}
}

export class CreateUser {
  static readonly type = '[Users] Create';
  constructor(public payload: User) {}
}

export class CreateUserSuccess {
  static readonly type = '[Users] Create Success';
  constructor(public payload: User) {}
}

export class CreateUserFailure {
  static readonly type = '[Users] Create Failure';
  constructor(public payload: any) {}
}

export class UserRedirect {
  static readonly type = '[Users] Redirect to list';
}
