import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  CreateUser,
  CreateUserFailure,
  CreateUserSuccess,
  LoadUsers,
  LoadUsersFailure,
  LoadUsersSuccesss,
  SaveUser,
  SaveUserFailure,
  SaveUserSuccess,
  UserRedirect,
} from './user.actions';
import { UserService } from '@app/users/services/user.service';
import { User } from '@app/users/models/user.model';
import { catchError, map, tap } from 'rxjs/internal/operators';
import { Navigate } from '@ngxs/router-plugin';
import { ShowError, ShowSuccess } from '@app/shared/store/app.actions';

export interface UserStateModel {
  list: User[];
}

export const USER_INITIAL_STATE = {
  list: null,
};

@State<UserStateModel>({
  name: 'user',
  defaults: USER_INITIAL_STATE,
})
export class UserState {
  constructor(
    private userService: UserService,
  ) {}

  @Selector()
  static getList(state: UserStateModel) { return state.list; }

  @Action(LoadUsers)
  loadUsers({ dispatch }: StateContext<UserStateModel>) {
    return this.userService
      .getList()
      .pipe(
        tap(res => dispatch(new LoadUsersSuccesss(res.data))),
        catchError(error => dispatch(new LoadUsersFailure(error))),
      );
  }

  @Action(LoadUsersSuccesss)
  getUsersSuccess({ patchState }: StateContext<UserStateModel>, { payload }: LoadUsersSuccesss) {
    patchState({ list: payload })
  }

  @Action(SaveUser)
  saveUser({ dispatch }: StateContext<UserStateModel>, { payload }: SaveUser) {
    return this.userService
      .save(payload)
      .pipe(
        tap(() => dispatch(new SaveUserSuccess(payload))),
        catchError(error => dispatch(new SaveUserFailure(error))),
      );
  }

  @Action(SaveUserSuccess)
  saveUserSuccess({ dispatch, getState, patchState }: StateContext<UserStateModel>, { payload }: SaveUserSuccess) {
    const { list } = getState();

    patchState({
      list: list.map(user => user._id === payload._id ? payload : user),
    });

    dispatch(new UserRedirect());
    dispatch(new ShowSuccess('Successfully saved'));
  }

  @Action(SaveUserFailure)
  saveUserFailure({ dispatch }: StateContext<UserStateModel>, { payload }: SaveUserFailure) {
    dispatch(new ShowError(payload));
  }

  @Action(CreateUser)
  createUser({ dispatch }: StateContext<UserStateModel>, { payload }: CreateUser) {
    return this.userService
      .create(payload)
      .pipe(
        tap(() => dispatch(new CreateUserSuccess(payload))),
        catchError(error => dispatch(new CreateUserFailure(error))),
      );
  }

  @Action(CreateUserSuccess)
  createUserSuccess({ dispatch, getState, patchState }: StateContext<UserStateModel>, { payload }: CreateUserSuccess) {
    const { list } = getState();

    list.push(payload);

    patchState({ list });

    dispatch(new UserRedirect());
    dispatch(new ShowSuccess('Successfully created'));
  }

  @Action(CreateUserFailure)
  createUserFailure({ dispatch }: StateContext<UserStateModel>, { payload }: CreateUserFailure) {
    dispatch(new ShowError(payload));
  }

  @Action(UserRedirect)
  userRedirect({ dispatch }: StateContext<UserStateModel>) {
    dispatch(new Navigate(['/admin/users']));
  }
}
