import { Routes } from '@angular/router';
import { UserListPageComponent } from '@app/users/containers/user-list-page/user-list-page.component';
import { UserEditPageComponent } from '@app/users/containers/user-edit-page/user-edit-page.component';
import { UsersActivator } from '@app/users/services/users.activator';

export const userRoutes: Routes = [
  {
    path: '',
    canActivate: [UsersActivator],
    children: [
      {
        path: '',
        component: UserListPageComponent,
      },
      {
        path: 'new',
        component: UserEditPageComponent,
      },
      {
        path: ':id',
        component: UserEditPageComponent,
      }
    ],
  },
];
