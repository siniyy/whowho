import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { UserState } from '@app/users/store/user.state';
import { filter, switchMap, tap } from 'rxjs/internal/operators';
import { Observable, of } from 'rxjs/index';
import { LoadUsers } from '@app/users/store/user.actions';
import { User } from '@app/users/models/user.model';
import { AppState } from '@app/shared/store/app.state';
import { RemoveForReload } from '@app/shared/store/app.actions';

@Injectable()
export class UsersActivator implements CanActivate {
  constructor(private store: Store) {}

  waitForDataToLoad(): Observable<User[]> {
    return this.store
      .select(UserState.getList)
      .pipe(
        filter(list => !!list),
      )
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const forReload = this.store.selectSnapshot(AppState.getForReload);

    if (!forReload.includes('users')) {
      return of(true);
    }

    this.store.dispatch(new LoadUsers());

    return this
      .waitForDataToLoad()
      .pipe(
        tap(() => this.store.dispatch(new RemoveForReload('users'))),
        switchMap(() => of(true)),
      );
  }
}