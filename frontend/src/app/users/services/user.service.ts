import { Injectable } from '@angular/core';
import { HttpService } from '@app/shared/services/http/http.service';
import { User } from '@app/users/models/user.model';

@Injectable()
export class UserService {
  url = 'admin/users';

  constructor(private httpService: HttpService) {}

  getList() {
    return this.httpService.get(this.url);
  }

  create(user: User) {
    return this.httpService.post(this.url, user);
  }

  save(user: User) {
    return this.httpService.put(`${this.url}/${user._id}`, user);
  }
}