import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authenticate } from '@app/auth/models/authenticate.model';
import { User } from '@app/users/models/user.model';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  @Input() user: User;

  type = 'New';

  form: FormGroup = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    isBlocked: [''],
    isModerator: [''],
    gender: [''],
    location: [''],
    birthday: [''],
  });

  @Output() submitted = new EventEmitter<Authenticate>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    if (this.user) {
      this.type = 'Edit';

      this.form.setValue({
        name: this.user.name,
        email: this.user.email,
        isBlocked: this.user.isBlocked,
        isModerator: this.user.isModerator,
        gender: this.user.gender || '',
        location: this.user.location || '',
        birthday: this.user.birthday || '',
      });
    }
  }

  onSubmit() {
    const data = this.user ? Object.assign(this.user, this.form.value) : this.form.value;
    this.submitted.emit(data);
  }
}
