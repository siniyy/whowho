export interface User {
  _id?: string;
  email: string;
  name: string;
  gender: string,
  location: string,
  birthday: string,
  is_admin: boolean,
  isBlocked: boolean,
  isModerator: boolean,
  created_at?: Date;
  updated_at?: Date;
}