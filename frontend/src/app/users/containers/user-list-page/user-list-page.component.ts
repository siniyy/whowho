import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

import { filter } from 'rxjs/internal/operators';
import { ModalService } from '@app/shared/components/ww-modal/modal.service';
import { UserState } from '@app/users/store/user.state';
import { SaveUser } from '@app/users/store/user.actions';

@Component({
  selector: 'app-user-list-page',
  templateUrl: './user-list-page.component.html',
  styleUrls: ['./user-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListPageComponent {
  columns = [
    {
      key: 'name',
      title: 'Name',
      extra: 'email',
    },
    {
      key: 'location',
      title: 'Location',
    },
    {
      key: 'isBlocked',
      title: 'Blocked',
      type: 'boolean',
    },
    {
      key: 'isModerator',
      title: 'Moderator',
      type: 'boolean',
    },
    {
      key: 'created_at',
      title: 'Created at',
      type: 'date',
    },
  ];
  options = {
    pagination: {
      pageSize: 10,
      pageSizeOptions: [10, 25, 50, 100]
    },
    actions: true,
    block: true,
  };

  @Select(UserState.getList) users$;

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private modalService: ModalService,
  ) {}

  onEdit(user) {
    this.store.dispatch(new Navigate([`/admin/users/${user._id}`]));
  }

  onBlock(user) {
    this.modalService
      .open('confirm', {
        title: `Sure you want to ${user.isBlocked ? 'un-' : ''}block user ${user.name} (${user.email})?`,
      })
      .pipe(filter(confirmed => confirmed))
      .subscribe(() => {
        user.isBlocked = !user.isBlocked;
        this.store.dispatch(new SaveUser(user))
      });
  }
}
