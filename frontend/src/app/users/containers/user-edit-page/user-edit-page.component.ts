import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { CreateUser, SaveUser } from '@app/users/store/user.actions';
import { UserState } from '@app/users/store/user.state';
import { User } from '@app/users/models/user.model';
import { Observable } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';

@Component({
  selector: 'app-user-edit-page',
  templateUrl: './user-edit-page.component.html',
  styleUrls: ['./user-edit-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserEditPageComponent {
  user$: Observable<User>;

  constructor(
    private store: Store,
    route: ActivatedRoute,
  ) {
    // TODO redirect if not found
    this.user$ = this.store
      .select(UserState.getList)
      .pipe(
        map(users => users.find(user => user._id === route.snapshot.params.id)),
      );
    }

  onSubmit(user) {
    if (user._id) {
      this.store.dispatch(new SaveUser(user));
    } else {
      this.store.dispatch(new CreateUser(user));
    }
  }
}
