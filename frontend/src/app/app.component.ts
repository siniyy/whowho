import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '@app/shared/animations/fade-in.animation';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  animations: [fadeInAnimation],
})
export class AppComponent {
  @HostBinding('@fadeIn') routeAnimation = true;

  constructor() {}
}
