export interface Topic {
  _id?: string;
  title: string;
  category_id: string;
  user_id: string;
  pic?: string;
  data?: any[];
  moderated: boolean;
  created_at?: Date;
  updated_at?: Date;
}