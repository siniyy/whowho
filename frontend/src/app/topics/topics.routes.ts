import { Routes } from '@angular/router';
import { TopicListPageComponent } from '@app/topics/containers/topic-list-page/topic-list-page.component';
import { TopicEditPageComponent } from '@app/topics/containers/topic-edit-page/topic-edit-page.component';
import { TopicsActivator } from '@app/topics/services/topics.activator';
import { CategoriesActivator } from '@app/categories/services/categories.activator';
import { UsersActivator } from '@app/users/services/users.activator';

export const topicRoutes: Routes = [
  {
    path: '',
    canActivate: [TopicsActivator, CategoriesActivator, UsersActivator],
    children: [
      {
        path: '',
        component: TopicListPageComponent,
      },
      {
        path: ':id',
        component: TopicEditPageComponent,
      }
    ],
  },
];
