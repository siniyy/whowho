import { Topic } from '@app/topics/models/topic.model';
import { Category } from '@app/categories/models/category.model';

export class LoadTopics {
  static readonly type = '[Topics] Load';
}

export class LoadTopicsSuccesss {
  static readonly type = '[Topics] Load Success';
  constructor(public payload: Topic[]) {}
}

export class LoadTopicsFailure {
  static readonly type = '[Topics] Load Failure';
  constructor(public payload: any) {}
}

export class SaveTopic {
  static readonly type = '[Topics] Save';
  constructor(public payload: Topic) {}
}

export class SaveTopicSuccess {
  static readonly type = '[Topics] Save Success';
  constructor(public payload: Topic) {}
}

export class SaveTopicFailure {
  static readonly type = '[Topics] Save Failure';
  constructor(public payload: any) {}
}

export class CreateTopic {
  static readonly type = '[Topics] Create';
  constructor(public payload: any, public shouldReset: boolean) {}
}

export class CreateTopicSuccess {
  static readonly type = '[Topics] Create Success';
  constructor(public payload: Topic, public shouldReset: boolean) {}
}

export class CreateTopicFailure {
  static readonly type = '[Topics] Create Failure';
  constructor(public payload: any) {}
}

export class RemoveTopic {
  static readonly type = '[Topics] Remove';
  constructor(public payload: string) {}
}

export class RemoveTopicSuccess {
  static readonly type = '[Topics] Remove  Success';
  constructor(public payload: string) {}
}

export class TopicRedirect {
  static readonly type = '[Topics] Redirect to list';
}

export class TopicModerate {
  static readonly type = '[Topics] Moderate';
  constructor(public id: string) {}
}

export class TopicModerateSuccess {
  static readonly type = '[Topics] Moderate Success';
  constructor(public id: string) {}
}

