import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  LoadTopics,
  LoadTopicsFailure,
  LoadTopicsSuccesss,
  RemoveTopic,
  RemoveTopicSuccess,
  SaveTopic,
  SaveTopicFailure,
  SaveTopicSuccess,
  TopicModerate,
  TopicRedirect,
  CreateTopic,
  CreateTopicFailure,
  CreateTopicSuccess,
  TopicModerateSuccess,
} from './topic.actions';
import { TopicService } from '@app/topics/services/topic.service';
import { Topic } from '@app/topics/models/topic.model';
import { catchError, tap } from 'rxjs/internal/operators';
import { Navigate } from '@ngxs/router-plugin';
import { ShowError, ShowSuccess } from '@app/shared/store/app.actions';

export interface TopicStateModel {
  list: Topic[];
  notModerated: number;
}

export const TOPIC_INITIAL_STATE = {
  list: null,
  notModerated: null,
};

@State<TopicStateModel>({
  name: 'topic',
  defaults: TOPIC_INITIAL_STATE,
})
export class TopicState {
  constructor(
    private topicService: TopicService,
  ) {}

  @Selector()
  static getList(state: TopicStateModel) { return state.list; }

  @Selector()
  static getNotModerated(state: TopicStateModel) { return state.notModerated; }

  @Action(LoadTopics)
  loadTopics({ dispatch }: StateContext<TopicStateModel>) {
    return this.topicService
      .getList()
      .pipe(
        tap(res => dispatch(new LoadTopicsSuccesss(res.data))),
        catchError(error => dispatch(new LoadTopicsFailure(error))),
      );
  }

  @Action(LoadTopicsSuccesss)
  getTopicsSuccess({ patchState }: StateContext<TopicStateModel>, { payload }: LoadTopicsSuccesss) {
    patchState({
      list: payload,
      notModerated: payload.filter(i => !i.moderated).length,
    })
  }

  @Action(TopicModerate)
  onApproveTopic({ dispatch, patchState }: StateContext<TopicStateModel>, { id }: TopicModerate) {
    return this.topicService
      .moderate(id)
      .pipe(
        tap(() => dispatch(new TopicModerateSuccess(id))),
      );
  }

  @Action(TopicModerateSuccess)
  onApproveTopicSuccess(
    { getState, patchState, dispatch }: StateContext<TopicStateModel>,
    { id }: TopicModerateSuccess,
  ) {
    const { list } = getState();

    const topic = list.find(topic => topic._id === id);

    if (topic) {
      topic.moderated = true;

      dispatch(new TopicRedirect());
      dispatch(new ShowSuccess('Successfully moderated'));
    }
  }
  ​
  @Action(RemoveTopic)
  removeTopic({ dispatch, patchState }: StateContext<TopicStateModel>, { payload }: RemoveTopic) {
    return this.topicService
      .remove(payload)
      .pipe(
        tap(() => dispatch(new RemoveTopicSuccess(payload))),
      );
  }

  @Action(RemoveTopicSuccess)
  removeTopicSuccess({ getState, patchState }: StateContext<TopicStateModel>, { payload }: RemoveTopicSuccess) {
    const { list } = getState();

    patchState({
      list: list.filter(topic => topic._id !== payload),
    })
  }

  @Action(SaveTopic)
  saveTopic({ dispatch }: StateContext<TopicStateModel>, { payload }: SaveTopic) {
    return this.topicService
      .save(payload)
      .pipe(
        tap(res => dispatch(new SaveTopicSuccess(res.data))),
        catchError(error => dispatch(new SaveTopicFailure(error))),
      );
  }

  @Action(SaveTopicSuccess)
  saveTopicSuccess({ dispatch, getState, patchState }: StateContext<TopicStateModel>, { payload }: SaveTopicSuccess) {
    const { list } = getState();

    patchState({
      list: list.map(topic => topic._id === payload._id ? Object.assign(topic, payload) : topic),
    });

    dispatch(new TopicRedirect());
    dispatch(new ShowSuccess('Successfully saved'));
  }

  @Action(SaveTopicFailure)
  saveTopicFailure({ dispatch }: StateContext<TopicStateModel>, { payload }: SaveTopicFailure) {
    dispatch(new ShowError(payload));
  }

  @Action(CreateTopic)
  createTopic({ dispatch }: StateContext<TopicStateModel>, { payload, shouldReset }: CreateTopic) {
    const purePayload = {};
    payload.forEach((val, key) => purePayload[key] = val);

    return this.topicService
      .create(payload)
      .pipe(
        tap(res => dispatch(new CreateTopicSuccess(Object.assign(purePayload, res.data), shouldReset))),
        catchError(error => dispatch(new SaveTopicFailure(error))),
      );
  }

  @Action(CreateTopicSuccess)
  createCategorySuccess({ dispatch, getState, patchState }: StateContext<TopicStateModel>, { payload, shouldReset }: CreateTopicSuccess) {
    const { list } = getState();

    console.log('topic.state.ts::createCategorySuccess::152 >>>> ', payload);
    list.push(payload);

    patchState({ list });

    if (!shouldReset) {
      dispatch(new TopicRedirect());
    }

    dispatch(new ShowSuccess('Successfully added'));
  }

  @Action(CreateTopicFailure)
  createCategoryFailure({ dispatch }: StateContext<TopicStateModel>, { payload }: CreateTopicFailure) {
    dispatch(new ShowError(payload));
  }

  @Action(TopicRedirect)
  topicRedirect({ dispatch }: StateContext<TopicStateModel>) {
    dispatch(new Navigate(['/admin/topics']));
  }
}
