import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Topic } from '@app/topics/models/topic.model';
import { Category } from '@app/categories/models/category.model';
import { User } from '@app/users/models/user.model';
import { filter } from 'rxjs/internal/operators';
import { Actions, ofActionSuccessful } from '@ngxs/store';
import { BehaviorSubject } from 'rxjs/index';
import { CreateTopicSuccess } from '@app/topics/store/topic.actions';

@Component({
  selector: 'app-topic-form',
  templateUrl: './topic-form.component.html',
  styleUrls: ['./topic-form.component.scss'],
})
export class TopicFormComponent implements OnInit {
  @Input() topic: Topic;
  @Input() categories: Category[];
  @Input() users: User[];

  @ViewChild('titleInput') titleInputEl: ElementRef;
  @ViewChild('fileInput') fileInputEl: ElementRef;

  pic$ = new BehaviorSubject('');

  data: string;

  form: FormGroup = this.fb.group({
    title: ['', Validators.required],
    data: [''],
    category_id: ['', Validators.required],
    user_id: ['', Validators.required],
    pic: ['', Validators.required],
    shouldReset: [false],
  });

  @Output() submitted = new EventEmitter();
  @Output() moderated = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private actions$: Actions,
  ) {
    this.actions$
      .pipe(
        ofActionSuccessful(CreateTopicSuccess),
        filter(() => this.form.get('shouldReset').value),
      )
      .subscribe(() => {
        this.pic$.next('');
        this.form.reset();

        this.fileInputEl.nativeElement.value = '';
        this.titleInputEl.nativeElement.focus();
      });
  }

  ngOnInit() {
    if (this.topic) {
      const pic = this.topic.pic || '';

      this.form.removeControl('data');

      this.form.setValue({
        pic,
        title: this.topic.title,
        category_id: this.topic.category_id,
        user_id: this.topic.user_id,
        shouldReset: false,
      });

      this.pic$.next(`/topics/${this.topic._id}/small/${pic}`);

      this.data = this.topic.data
        .filter(row => row.type !== 'poll') // TODO remove after imlementing poll editing
        .map((row) => {
          let output;

          switch (row.type) {
            case 'img':
              output = `<img src='/topics/${this.topic._id}/small/${row.data}'>`;
              break;

            case 'poll':
              // TODO add poll editing
              break;

            default: //text
              output = row.data;
          }

          return `<p>${output}</p>`;
        })
        .join('<br>');
    }
  }

  onFileChange(event) {
    const files = event.target.files;

    if (files.length > 0) {
      const file = files[0];

      this.form.get('pic').setValue(file);

      this.pic$.next(URL.createObjectURL(file));
    }
  }

  private prepareSave(): any {
    let input = new FormData();

    if (this.topic) {
      input.append('_id', this.topic._id);
    } else {
      const data = this.form.get('data').value;

      if (data) {
        input.append('data', JSON.stringify([{ data, type: 'text' }]));
      }

      input.append('moderated', 'true');
    }

    input.append('title', this.form.get('title').value);
    input.append('pic', this.form.get('pic').value);
    input.append('category_id', this.form.get('category_id').value);
    input.append('user_id', this.form.get('user_id').value);

    return input;
  }

  onModerate(id) {
    this.moderated.emit(id);
  }

  onSubmit() {
    const topic = this.prepareSave();

    this.submitted.emit({
      topic,
      shouldReset: this.form.get('shouldReset').value,
    });
  }
}
