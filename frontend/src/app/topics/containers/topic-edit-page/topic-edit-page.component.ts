import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { CreateTopic, SaveTopic, TopicModerate } from '@app/topics/store/topic.actions';
import { TopicState } from '@app/topics/store/topic.state';
import { Topic } from '@app/topics/models/topic.model';
import { Observable } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';
import { CategoryState } from '@app/categories/store/category.state';
import { UserState } from '@app/users/store/user.state';

@Component({
  selector: 'app-topic-edit-page',
  templateUrl: './topic-edit-page.component.html',
  styleUrls: ['./topic-edit-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopicEditPageComponent {
  topic$: Observable<Topic>;

  @Select(CategoryState.getList) categories$;
  @Select(UserState.getList) users$;

  constructor(
    private store: Store,
    route: ActivatedRoute,
  ) {
    // TODO redirect if not found
    this.topic$ = this.store
      .select(TopicState.getList)
      .pipe(
        map(topics => topics.find(topic => topic._id === route.snapshot.params.id)),
      );
  }

  onModerate(id) {
    this.store.dispatch(new TopicModerate(id));
  }

  onSubmit(data) {
    if (data.topic.get('_id')) {
      this.store.dispatch(new SaveTopic(data.topic));
    } else {
      this.store.dispatch(new CreateTopic(data.topic, data.shouldReset));
    }
  }
}
