import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

import { filter } from 'rxjs/internal/operators';
import { ModalService } from '@app/shared/components/ww-modal/modal.service';
import { RemoveTopic } from '@app/topics/store/topic.actions';
import { TopicState } from '@app/topics/store/topic.state';

@Component({
  selector: 'app-topic-list-page',
  templateUrl: './topic-list-page.component.html',
  styleUrls: ['./topic-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopicListPageComponent {
  columns = [
    {
      key: 'pic',
      title: 'Pic',
      type: 'pic',
      path: 'topics',
    },
    {
      key: 'title',
      title: 'Title',
    },
    {
      key: 'moderated',
      title: 'In review',
      type: 'boolean-reverse',
    },
    {
      key: 'created_at',
      title: 'Created at',
      type: 'date',
    },
    {
      key: 'updated_at',
      title: 'Updated at',
      type: 'date',
    },
  ];
  options = {
    pagination: {
      pageSize: 10,
      pageSizeOptions: [10, 25, 50, 100]
    },
    actions: true,
    sortable: {
      column: 'moderated',
      value: 'asc',
    }
  };

  @Select(TopicState.getList) topics$;

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private modalService: ModalService,
  ) {}

  onEdit(topic) {
    this.store.dispatch(new Navigate([`/admin/topics/${topic._id}`]));
  }

  onDelete(topic) {
    this.modalService
      .open('confirm', {
        title: `Sure you want to delete topic ${topic.title}?`,
      })
      .pipe(filter(confirmed => confirmed))
      .subscribe(() => this.store.dispatch(new RemoveTopic(topic._id)));
  }
}
