import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { TopicState } from 'src/app/topics/store/topic.state';
import { filter, switchMap, tap } from 'rxjs/internal/operators';
import { Observable, of } from 'rxjs/index';
import { LoadTopics } from 'src/app/topics/store/topic.actions';
import { Topic } from 'src/app/topics/models/topic.model';
import { RemoveForReload } from '@app/shared/store/app.actions';
import { AppState } from '@app/shared/store/app.state';

@Injectable()
export class TopicsActivator implements CanActivate {
  constructor(private store: Store) {}

  waitForDataToLoad(): Observable<Topic[]> {
    return this.store
      .select(TopicState.getList)
      .pipe(
        filter(list => !!list),
      )
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const forReload = this.store.selectSnapshot(AppState.getForReload);

    if (!forReload.includes('topics')) {
      return of(true);
    }

    this.store.dispatch(new LoadTopics());

    return this
      .waitForDataToLoad()
      .pipe(
        tap(() => this.store.dispatch(new RemoveForReload('topics'))),
        switchMap(() => of(true)),
      );
  }
}