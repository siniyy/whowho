import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { Topic } from 'src/app/topics/models/topic.model';

@Injectable()
export class TopicService {
  url = 'admin/topics';

  constructor(private httpService: HttpService) {}

  getList() {
    return this.httpService.get(this.url);
  }

  create(topic) {
    return this.httpService.post(this.url, topic);
  }

  save(topic) {
    return this.httpService.put(`${this.url}/${topic.get('_id')}`, topic);
  }

  remove(id) {
    return this.httpService.delete(`${this.url}/${id}`)
  }

  moderate(id) {
    return this.httpService.post(`${this.url}/moderate`, { id });
  }
}