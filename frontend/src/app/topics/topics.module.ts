import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { MaterialModule } from '@app/shared/material';
import { ComponentsModule } from '@app/shared/components';

import { TopicListPageComponent } from '@app/topics/containers/topic-list-page/topic-list-page.component';
import { TopicEditPageComponent } from '@app/topics/containers/topic-edit-page/topic-edit-page.component';
import { TopicFormComponent } from '@app/topics/components/topic-form-component/topic-form.component';

import { TopicState } from '@app/topics/store/topic.state';
import { TopicsActivator } from '@app/topics/services/topics.activator';
import { TopicService } from '@app/topics/services/topic.service';

import { topicRoutes } from '@app/topics/topics.routes';

export const COMPONENTS = [
  TopicListPageComponent,
  TopicEditPageComponent,
  TopicFormComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    ComponentsModule,
    RouterModule.forChild(topicRoutes),

    NgxsFormPluginModule,
    NgxsModule.forFeature([TopicState]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [
    TopicsActivator,
    TopicService,
  ],
})
export class TopicsModule {
}