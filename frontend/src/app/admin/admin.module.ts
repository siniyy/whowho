import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { adminRoutes } from '@app/admin/admin.routes';
import { AdminComponent } from '@app/admin/containers/admin.component';
import { AdminToolbarComponent } from '@app/admin/components/toolbar/admin-toolbar.component';
import { MaterialModule } from '@app/shared/material';
import { AdminActivator } from '@app/admin/services/admin.activator';

import { NgxsModule } from '@ngxs/store';
import { CategoryState } from '@app/categories/store/category.state';
import { CategoryService } from '@app/categories/services/category.service';
import { AdminNavItemComponent } from '@app/admin/components/nav-item/admin-nav-item.component';
import { CategoriesActivator } from '@app/categories/services/categories.activator';
import { UserState } from '@app/users/store/user.state';
import { UsersActivator } from '@app/users/services/users.activator';
import { UserService } from '@app/users/services/user.service';
import { TopicService } from '@app/topics/services/topic.service';
import { TopicsActivator } from '@app/topics/services/topics.activator';

@NgModule({
  declarations: [
    AdminComponent,
    AdminToolbarComponent,
    AdminNavItemComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,

    RouterModule.forChild(adminRoutes),

    NgxsModule.forFeature([
      CategoryState,
      UserState,
    ]),

  ],
  providers: [
    AdminActivator,
    CategoriesActivator,
    CategoryService,
    UsersActivator,
    UserService,
    TopicsActivator,
    TopicService,
  ],
})
export class AdminModule {
}