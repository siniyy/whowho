import { Component } from '@angular/core';
import { TopicState } from '@app/topics/store/topic.state';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-admin-toolbar',
  templateUrl: './admin-toolbar.component.html',
  styleUrls: ['./admin-toolbar.component.scss'],
})
export class AdminToolbarComponent {
  navItems = [
    {
      title: 'Topics',
      path: '/admin/topics',
      badge$: this.store.select(TopicState.getNotModerated)
    },
    {
      title: 'Categories',
      path: '/admin/categories',
    },
    {
      title: 'Users',
      path: '/admin/users',
    },
    // {
    //   title: 'Settings',
    //   path: '/admin/settings',
    // },
  ];

  constructor(private store: Store) {}
}
