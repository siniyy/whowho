import { Component, Input, OnInit } from '@angular/core';
import { of } from 'rxjs/index';

@Component({
  selector: 'app-admin-nav-item',
  templateUrl: './admin-nav-item.component.html',
  styleUrls: ['./admin-nav-item.component.scss'],
})
export class AdminNavItemComponent implements OnInit {
  @Input() data: any;

  ngOnInit() {
    if (!this.data.badge$) {
      this.data.badge$ = of(null);
    }
  }
}
