import { Component } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  constructor(private store: Store) {}
}
