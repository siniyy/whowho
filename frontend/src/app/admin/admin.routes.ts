import { Routes } from '@angular/router';
import { AdminComponent } from '@app/admin/containers/admin.component';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'topics',
        pathMatch: 'full'
      },
      {
        path: 'topics',
        loadChildren: '../topics/topics.module#TopicsModule',
      },
      {
        path: 'categories',
        loadChildren: '../categories/categories.module#CategoriesModule',
      },
      {
        path: 'users',
        loadChildren: '../users/users.module#UsersModule',
      },
      // {
      //   path: 'settings',
      //   loadChildren: '../settings/settings.module#SettingsModule',
      // },
    ],
  },
];
