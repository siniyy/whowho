import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Authenticate } from '@app/auth/models/authenticate.model';
import { fadeAnimation } from '@app/shared/animations/fade.animation';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  animations: [fadeAnimation],
})
export class LoginFormComponent {
  @Input() loading: boolean;

  @Output() submitted = new EventEmitter<Authenticate>();

  form: FormGroup = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) {}

  onSubmit() {
    this.submitted.emit(this.form.value);
  }
}
