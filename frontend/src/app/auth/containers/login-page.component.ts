import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store, Select } from '@ngxs/store';

import { AuthPageState } from 'src/app/auth/store/auth-page.state';
import { Login } from 'src/app/auth/store/auth.actions';
import { Authenticate } from '@app/auth/models/authenticate.model';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginPageComponent {
  @Select(AuthPageState.getLoading) loading$;

  constructor(
    private store: Store,
  ) {}

  onSubmit(form: Authenticate) {
    this.store.dispatch(new Login(form));
  }
}
