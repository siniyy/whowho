import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Authenticate } from '@app/auth/models/authenticate.model';
import { Admin } from '@app/auth/models/admin.model';
import { HttpService } from '@app/shared/services/http/http.service';
import { map } from 'rxjs/internal/operators';

@Injectable()
export class AuthService {
  constructor(private httpService: HttpService) {}

  login(data: Authenticate): Observable<Admin> {
    return this.httpService
      .post('auth/login', data, { observe: 'response' })
      .pipe(
        map((res) => {
          return {
            name: res.body.data.name,
            token: res.headers.get('token'),
          };
        }),
      );
  }

  logout(): Observable<boolean> {
    return this.httpService.post('auth/logout');
  }
}
