import { Action, Selector, State, StateContext } from '@ngxs/store';

import { Admin } from '@app/auth/models/admin.model';
import { LoginSuccess, Logout } from '@app/auth/store/auth.actions';

export interface AdminStateModel {
  user: Admin;
  isLoggedIn: boolean;
}

const INITIAL_STATE = {
  user: null,
  isLoggedIn: false,
};

@State<AdminStateModel>({
  name: 'admin',
  defaults: INITIAL_STATE,
})
export class AdminState {
  @Selector()
  static getLoggedIn(state: AdminStateModel) { return state.isLoggedIn; }

  @Selector()
  static getUser(state: AdminStateModel) { return state.user; }

  @Action(LoginSuccess)
  loginSuccess({ patchState }: StateContext<AdminStateModel>, { user }: LoginSuccess) {
    patchState({
      user,
      isLoggedIn: true,
    });
  }
​
  // FIXME add LogoutSuccess ?
  @Action(Logout)
  logout({ setState }: StateContext<AdminStateModel>) {
    setState(INITIAL_STATE);
  }
}
