import { Authenticate } from '@app/auth/models/authenticate.model';
import { Admin } from '@app/auth/models/admin.model';

export class Login {
  static readonly type = '[Auth] Login';
  constructor(public payload: Authenticate) {}
}

export class LoginSuccess {
  static readonly type = '[Auth] Login Success';
  constructor(public user: Admin) {}
}

export class LoginFailure {
  static readonly type = '[Auth] Login Failure';
  constructor(public payload: any) {}
}

export class Logout {
  static readonly type = '[Auth] Logout';
}

export class LoginRedirect {
  static readonly type = '[Auth] Login Redirect';
}
