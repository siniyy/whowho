import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { catchError, tap } from 'rxjs/internal/operators';
import {
  Login,
  LoginFailure,
  LoginSuccess,
  Logout,
  LoginRedirect,
} from '@app/auth/store/auth.actions';
import { AuthService } from '@app/auth/services/auth.service';
import { TopicRedirect } from '@app/topics/store/topic.actions';
import { ShowError } from '@app/shared/store/app.actions';

export interface AuthPageStateModel {
  loading: boolean;
}

export const AUTH_PAGE_INITIAL_STATE = {
  loading: false,
};

@State<AuthPageStateModel>({
  name: 'authPage',
  defaults: AUTH_PAGE_INITIAL_STATE,
})
export class AuthPageState {
  constructor(
    private authService: AuthService,
  ) {}

  @Selector()
  static getLoading(state: AuthPageStateModel) { return state.loading; }
​
  @Action(Login)
  login({ dispatch, patchState }: StateContext<AuthPageStateModel>, { payload }: Login) {
    patchState({ loading: true });

    return this.authService
      .login(payload)
      .pipe(
        tap(user => dispatch(new LoginSuccess(user))),
        catchError(error => dispatch(new LoginFailure(error))),
      );
  }

  @Action(LoginSuccess)
  loginSuccess({ dispatch, patchState }: StateContext<AuthPageStateModel>) {
    patchState({ loading: false });

    dispatch(new Navigate(['/admin/topics']));
  }
​
  @Action(LoginFailure)
  loginFailure({ dispatch, patchState }: StateContext<AuthPageStateModel>, { payload }: LoginFailure) {
    dispatch(new ShowError(payload));
    patchState({ loading: false });
  }

  @Action(LoginRedirect)
  loginRedirect({ dispatch }: StateContext<AuthPageStateModel>) {
    dispatch(new Navigate(['/login']));
  }

  @Action(Logout)
  logout() {
    return this.authService.logout();
  }
}
