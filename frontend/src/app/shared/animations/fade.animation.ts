import { animate, style, transition, trigger } from '@angular/animations';

export const fadeAnimation = trigger('fade', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('400ms ease-in-out', style({ opacity: 1 })),
  ]),
  transition(':leave', [
    animate('400ms ease-in-out', style({ opacity: 0 })),
  ]),
]);
