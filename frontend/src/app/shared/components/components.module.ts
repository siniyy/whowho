import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@app/shared/material';

import { SnackBarService } from '@app/shared/components/ww-snackbar/snackbar.service';

import { WWButtonComponent } from '@app/shared/components/ww-button/ww-button.component';
import { WWTableComponent } from '@app/shared/components/ww-table/ww-table.component';
import { WWSpinnerComponent } from '@app/shared/components/ww-spinner/ww-spinner.component';
import { WWSnackBarComponent } from '@app/shared/components/ww-snackbar/ww-snackbar.component';
import { WWModalComponent } from '@app/shared/components/ww-modal/ww-modal.component';
import { ModalService } from '@app/shared/components/ww-modal/modal.service';
import { SafeUrlPipe } from '@app/shared/pipes/safe-url.pipe';

const COMPONENTS = [
  WWButtonComponent,
  WWModalComponent,
  WWTableComponent,
  WWSpinnerComponent,
  WWSnackBarComponent,
  SafeUrlPipe,
];

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MaterialModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [
    ModalService,
    SnackBarService,
  ],
  entryComponents: [
    WWModalComponent,
    WWSnackBarComponent,
  ]
})
export class ComponentsModule {
}
