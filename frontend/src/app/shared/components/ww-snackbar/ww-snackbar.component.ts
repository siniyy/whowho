import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material';

@Component({
  selector: 'ww-snackbar',
  templateUrl: 'ww-snackbar.component.html',
})
export class WWSnackBarComponent {
  constructor(
    private snackBarRef: MatSnackBarRef<WWSnackBarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any,
  ) {}

  close() {
    this.snackBarRef.dismiss();
  }
}
