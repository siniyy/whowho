import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

import { WWSnackBarComponent } from '@app/shared/components/ww-snackbar/ww-snackbar.component';

const DEFAULT_CONFIG: MatSnackBarConfig = {
  duration: 3000,
  verticalPosition: 'top',
  panelClass: 'error',
};

@Injectable()
export class SnackBarService {
  constructor(private snackBar: MatSnackBar) {}

  public open(data: string | any = '') {
    const config = Object.assign({}, DEFAULT_CONFIG);

    if (typeof data === 'string') {
      config.data = { message: data };
    } else {
      config.data = data;
    }

    // remove duration if action button provided
    if (data.action) {
      config.duration = 0;
    }

    if (data.class) {
      config.panelClass = data.class;
    }

    this.snackBar.openFromComponent(WWSnackBarComponent, config);
  }
}
