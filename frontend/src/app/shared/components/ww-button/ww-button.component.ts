import { Component } from '@angular/core';
import { EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ww-button',
  templateUrl: './ww-button.component.html',
})
export class WWButtonComponent {
  @Input() icon: string;
  @Input() tooltip: string;

  @Input() color = 'primary';
  @Input() type = 'button';
  @Input() tabindex =  '-1';
  @Input() disabled =  false;

  @Output() clicked = new EventEmitter();
}
