import { Component, Input } from '@angular/core';

@Component({
  selector: 'ww-spinner',
  templateUrl: './ww-spinner.component.html',
  styleUrls: ['./ww-spinner.component.scss'],
})
export class WWSpinnerComponent {
  @Input() diameter: Number = 50;
}
