import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'ww-modal',
  templateUrl: 'ww-modal.component.html',
  styleUrls: ['ww-modal.component.scss'],
})
export class WWModalComponent {
  constructor(
    private dialogRef: MatDialogRef<WWModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  close(data?): void {
    this.dialogRef.close(data);
  }
}
