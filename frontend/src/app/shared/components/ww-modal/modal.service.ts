import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';

import { WWModalComponent } from './ww-modal.component';
import { modalsConfig } from './modals.config';

@Injectable()
export class ModalService {
  private dialogRef: MatDialogRef<any>;

  constructor(private dialog: MatDialog) {}

  public open(type: string, data: any = {}): Observable<any> {
    const config: MatDialogConfig = Object.assign({ data }, modalsConfig[type]);

    this.dialogRef = this.dialog.open(WWModalComponent, config);

    return this.dialogRef.beforeClose();
  }

  public close(): void {
    this.dialogRef.close();
  }
}
