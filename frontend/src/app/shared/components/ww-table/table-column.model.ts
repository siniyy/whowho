export interface TableColumn {
  key: string;
  title?: string;
  sortable?: boolean;
  type?: string;
}
