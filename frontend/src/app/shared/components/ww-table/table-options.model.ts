export interface TableOptions {
  sortable?: {
    column: string;
    direction: string;
  };
  pagination?: {
    pageSize: number;
    pageSizeOptions: number;
  };
  filter?: any;
  actions?: boolean;
}

