import { Observable } from 'rxjs/index';
import { TopicState } from '@app/topics/store/topic.state';
import { Store } from '@ngxs/store';
import { DataSource } from '@angular/cdk/collections';

export class StoreDataSource extends DataSource<any> {
  constructor(private store: Store) {
    super();
  }
  connect(): Observable<any[]> {
    return this.store.select(TopicState.getList);
  }
  disconnect() {}
}
