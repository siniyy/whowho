import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSortable, MatTableDataSource } from '@angular/material';

import { TableOptions } from './table-options.model';
import { TableColumn } from './table-column.model';

@Component({
  selector: 'ww-table',
  templateUrl: './ww-table.component.html',
  styleUrls: ['./ww-table.component.scss'],
})
export class WWTableComponent implements OnInit {
  displayedColumns: string[];
  dataSource: MatTableDataSource<any>;
  data: any;

  @Input() options: TableOptions = {};
  @Input() columns: TableColumn[];
  @Input('rows')
  set rows(data: any) {
    this.data = data;

    if (this.dataSource) { // FIXME very stupid
      this.dataSource.data = data;
    }
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Output() edited = new EventEmitter();
  @Output() deleted = new EventEmitter();

  ngOnInit() {
    this.applyOptions();

    this.displayedColumns = this.columns.map(i => i.key);

    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  trackById(index, item) {
    return item._id;
  }

  applyOptions() {
    if (this.options.actions) {
      this.columns.push({
        title: '',
        key: 'actions',
        type: 'actions'
      });
    }

    if (this.options.sortable) {
      this.sort.sort(<MatSortable>{
          id: this.options.sortable.column,
          start: this.options.sortable.direction
        }
      );
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

