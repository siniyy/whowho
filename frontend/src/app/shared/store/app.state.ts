import { Action, Selector, State, StateContext } from '@ngxs/store';

import { AddForReload, RemoveForReload, ToggleLoader } from '@app/shared/store/app.actions';

export interface AppStateModel {
  loading: boolean;
  reload: string[];
}

const INITIAL_STATE = {
  loading: false,
  reload: ['users', 'topics', 'categories'],
};

@State<AppStateModel>({
  name: 'app',
  defaults: INITIAL_STATE,
})
export class AppState {
  @Selector()
  static getLoading(state: AppStateModel) { return state.loading; }

  @Selector()
  static getForReload(state: AppStateModel) { return state.reload; }

  @Action(ToggleLoader)
  toggleLoader({ patchState }: StateContext<AppStateModel>, { payload }: ToggleLoader) {
    patchState({
      loading: payload,
    })
  }

  @Action(AddForReload)
  addForReload({ getState, patchState }: StateContext<AppStateModel>, { payload }: AddForReload) {
    const { reload } = getState();

    patchState({
      reload: reload
        .concat(payload)
        .filter((v, i, arr) => arr.indexOf(v) === i), // unique
    });
  }

  @Action(RemoveForReload)
  removeForReload({ getState, patchState }: StateContext<AppStateModel>, { payload }: RemoveForReload) {
    const { reload } = getState();

    patchState({
      reload: reload.filter(i => i !== payload),
    });
  }
}
