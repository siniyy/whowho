export class ShowError {
  static readonly type = '[App] Show error';
  constructor(public payload: string) {}
}

export class ShowSuccess {
  static readonly type = '[App] Show success';
  constructor(public payload: string) {}
}

export class ToggleLoader {
  static readonly type = '[App] Show loader';
  constructor(public payload: boolean) {}
}

export class AddForReload {
  static readonly type = '[App] Add for reload';
  constructor(public payload: string) {}
}

export class RemoveForReload {
  static readonly type = '[App] Remove for reload';
  constructor(public payload: string) {}
}
