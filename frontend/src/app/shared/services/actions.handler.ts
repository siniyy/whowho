import { Injectable } from '@angular/core';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { SnackBarService } from '@app/shared/components/ww-snackbar/snackbar.service';
import { ShowError, ShowSuccess } from '@app/shared/store/app.actions';

@Injectable()
export class ActionsHandler {
  constructor(
    private actions$: Actions,
    private store: Store,
    private snackBarService: SnackBarService,
  ) {
    this.actions$
      .pipe(
        ofActionDispatched(
          ShowSuccess,
        ),
      )
      .subscribe(({ payload }) => this.showSuccess(payload));

    this.actions$
      .pipe(
        ofActionDispatched(
          ShowError,
        ),
      )
      .subscribe(({ payload }) => this.showError(payload));
  }

  private showSuccess(payload) {
    this.snackBarService.open({
      message: payload,
      class: 'success',
    });
  }

  private showError(payload) {
    this.snackBarService.open(payload.error);
  }
}
