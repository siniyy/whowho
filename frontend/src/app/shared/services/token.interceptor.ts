import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AdminState } from '@app/auth/store/admin.state';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private store: Store) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.store.selectSnapshot(AdminState.getUser);

    if (user) {
      const requestWithToken = request.clone({
        setHeaders: {
          Authorization: `Bearer ${user.token}`,
        },
      });

      return next.handle(requestWithToken);
    }

    return next.handle(request);
  }
}
