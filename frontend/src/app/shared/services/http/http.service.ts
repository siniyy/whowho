import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { RequestOptions } from './request-options.model';
import { catchError } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private baseUrl = '/api/';

  private composeUrl(url: string, usePrefix = true) {
    return this.baseUrl + url;
  }

  constructor(private http: HttpClient) {}

  public get(url: string, options: RequestOptions = {}): Observable<any> {
    return this.http
      .get(this.composeUrl(url))
      .pipe(
        catchError(this.handleError),
      );
  }

  public post(url: string, body: any = {}, options: any = {}): Observable<any> {
    return this.http
      .post(this.composeUrl(url), body, options)
      .pipe(
        catchError(this.handleError),
      );
  }

  public put(url: string, body: any = {}, options: any = {}): Observable<any> {
    return this.http
      .put(this.composeUrl(url), body, options)
      .pipe(
        catchError(this.handleError),
      );
  }

  public delete(url: string, options: any = {}): Observable<any> {
    return this.http
      .delete(this.composeUrl(url), options)
      .pipe(
        catchError(this.handleError),
      );
  }

  // returns error object from HttpErrorResponse instance
  private handleError(rawError) {
    let error: any;

    if (rawError instanceof HttpErrorResponse) {
      error = rawError.error || {};
    } else {
      error = rawError.message ? rawError.message : JSON.stringify(rawError);
    }

    return throwError(error);
  }
}
