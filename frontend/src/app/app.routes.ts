import { Routes } from '@angular/router';
import { AuthGuard } from '@app/auth/services/auth.guard';
import { NotFoundPageComponent } from '@app/not-found-page/not-found-page.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/admin/topics',
    pathMatch: 'full'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    component: NotFoundPageComponent,
  },
];
