import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { CategoryRedirect, CreateCategory, SaveCategory } from '@app/categories/store/category.actions';
import { CategoryState } from '@app/categories/store/category.state';
import { Observable, of } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';
import { Category } from '@app/categories/models/category.model';

@Component({
  selector: 'app-category-edit-page',
  templateUrl: './category-edit-page.component.html',
  styleUrls: ['./category-edit-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryEditPageComponent {
  category$: Observable<Category>;
  type = 'edit';

  constructor(
    private store: Store,
    route: ActivatedRoute,
  ) {
    const id = route.snapshot.params.id;

    if (!id) {
      this.type = 'new';
      return;
    }

    this.category$ = this.store
      .select(CategoryState.getList)
      .pipe(
        map(categories => categories.find(category => category._id === id)),
      );
    }

  onSubmit(data) {
    if (data.category.get('_id')) {
      this.store.dispatch(new SaveCategory(data.category));
    } else {
      this.store.dispatch(new CreateCategory(data.category, data.shouldReset));
    }
  }
}
