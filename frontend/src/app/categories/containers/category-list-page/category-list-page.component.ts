import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

import { filter } from 'rxjs/internal/operators';
import { ModalService } from '@app/shared/components/ww-modal/modal.service';
import { CategoryState } from '@app/categories/store/category.state';
import { RemoveCategory } from '@app/categories/store/category.actions';

@Component({
  selector: 'app-category-list-page',
  templateUrl: './category-list-page.component.html',
  styleUrls: ['./category-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryListPageComponent {
  columns = [
    {
      key: 'pic',
      title: 'Pic',
      type: 'pic',
      path: 'categories',
    },
    {
      key: 'title',
      title: 'Title',
    },
    {
      key: 'posts_qty',
      title: 'Topics',
    },
  ];
  options = {
    pagination: {
      pageSize: 10,
      pageSizeOptions: [10, 25, 50, 100]
    },
    actions: true,
  };

  @Select(CategoryState.getList) categories$;

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private modalService: ModalService,
  ) {}

  onEdit(category) {
    this.store.dispatch(new Navigate([`/admin/categories/${category._id}`]));
  }

  onDelete(category) {
    this.modalService
      .open('confirm', {
        title: `Sure you want to delete category ${category.title}?`,
      })
      .pipe(filter(confirmed => confirmed))
      .subscribe(() => this.store.dispatch(new RemoveCategory(category._id)));
  }
}
