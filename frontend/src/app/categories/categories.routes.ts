import { Routes } from '@angular/router';
import { CategoryListPageComponent } from '@app/categories/containers/category-list-page/category-list-page.component';
import { CategoryEditPageComponent } from '@app/categories/containers/category-edit-page/category-edit-page.component';
import { CategoriesActivator } from '@app/categories/services/categories.activator';

export const categoriesRoutes: Routes = [
  {
    path: '',
    canActivate: [CategoriesActivator],
    children: [
      {
        path: '',
        component: CategoryListPageComponent,
      },
      {
        path: 'new',
        component: CategoryEditPageComponent,
      },
      {
        path: ':id',
        component: CategoryEditPageComponent,
      }
    ],
  },
];
