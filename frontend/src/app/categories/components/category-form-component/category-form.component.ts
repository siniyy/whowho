import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Category } from '@app/categories/models/category.model';
import { Actions, ofActionSuccessful } from '@ngxs/store';
import { CreateCategorySuccess } from '@app/categories/store/category.actions';
import { filter } from 'rxjs/internal/operators';
import { BehaviorSubject } from 'rxjs/index';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss'],
})
export class CategoryFormComponent implements OnInit {
  @Input() category: Category;
  @Input() type: string;

  @ViewChild('titleInput') titleInputEl: ElementRef;
  @ViewChild('fileInput') fileInputEl: ElementRef;

  pic$ = new BehaviorSubject('');

  form: FormGroup = this.fb.group({
    title: ['', Validators.required],
    pic: ['', Validators.required],
    shouldReset: [false],
  });

  @Output() submitted = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private actions$: Actions,
  ) {
    this.actions$
      .pipe(
        ofActionSuccessful(CreateCategorySuccess),
        filter(() => this.form.get('shouldReset').value),
      )
      .subscribe(() => {
        this.pic$.next('');
        this.form.reset();

        this.fileInputEl.nativeElement.value = '';
        this.titleInputEl.nativeElement.focus();
      });
  }

  ngOnInit() {
    if (this.category) {
      const pic = this.category.pic || '';

      this.form.setValue({
        pic,
        title: this.category.title,
        shouldReset: false,
      });

      this.pic$.next(`/categories/${this.category._id}/small/${pic}`);
    }
  }

  onFileChange(event) {
    const files = event.target.files;

    if (files.length > 0) {
      const file = files[0];

      this.form.get('pic').setValue(file);

      this.pic$.next(URL.createObjectURL(file));
    }
  }

  private prepareSave(): any {
    let input = new FormData();

    if (this.category) {
      input.append('_id', this.category._id);
    }

    input.append('title', this.form.get('title').value);
    input.append('pic', this.form.get('pic').value);

    return input;
  }

  onSubmit() {
    const category = this.prepareSave();

    this.submitted.emit({
      category,
      shouldReset: this.form.get('shouldReset').value
    });
  }
}
