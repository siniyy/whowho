export interface Category {
  _id?: string;
  title: string;
  pic: string;
  created_at?: Date;
  updated_at?: Date;
}