import { Injectable } from '@angular/core';
import { HttpService } from '@app/shared/services/http/http.service';
import { Category } from '@app/categories/models/category.model';

@Injectable()
export class CategoryService {
  url = 'admin/categories';

  constructor(private httpService: HttpService) {}

  getList() {
    return this.httpService.get(this.url);
  }

  create(category) {
    return this.httpService.post(this.url, category);
  }

  save(category) {
    return this.httpService.put(`${this.url}/${category.get('_id')}`, category);
  }

  remove(id) {
    return this.httpService.delete(`${this.url}/${id}`)
  }
}