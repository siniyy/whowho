import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { CategoryState } from '@app/categories/store/category.state';
import { filter, switchMap, tap } from 'rxjs/internal/operators';
import { Observable, of } from 'rxjs/index';
import { LoadCategories } from '@app/categories/store/category.actions';
import { Category } from '@app/categories/models/category.model';
import { RemoveForReload } from '@app/shared/store/app.actions';
import { AppState } from '@app/shared/store/app.state';

@Injectable()
export class CategoriesActivator implements CanActivate {
  constructor(private store: Store) {}

  waitForDataToLoad(): Observable<Category[]> {
    return this.store
      .select(CategoryState.getList)
      .pipe(
        filter(list => !!list),
      );
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const forReload = this.store.selectSnapshot(AppState.getForReload);

    if (!forReload.includes('categories')) {
      return of(true);
    }

    this.store.dispatch(new LoadCategories());

    return this
      .waitForDataToLoad()
      .pipe(
        tap(() => this.store.dispatch(new RemoveForReload('categories'))),
        switchMap(() => of(true)),
      );
  }
}