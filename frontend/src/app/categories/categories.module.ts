import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { MaterialModule } from '@app/shared/material';
import { ComponentsModule } from '@app/shared/components';

import { categoriesRoutes } from '@app/categories/categories.routes';
import { CategoryState } from '@app/categories/store/category.state';
import { CategoryService } from '@app/categories/services/category.service';
import { CategoriesActivator } from '@app/categories/services/categories.activator';
import { CategoryListPageComponent } from '@app/categories/containers/category-list-page/category-list-page.component';
import { CategoryEditPageComponent } from '@app/categories/containers/category-edit-page/category-edit-page.component';
import { CategoryFormComponent } from '@app/categories/components/category-form-component/category-form.component';

export const COMPONENTS = [
  CategoryListPageComponent,
  CategoryEditPageComponent,
  CategoryFormComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    ComponentsModule,
    RouterModule.forChild(categoriesRoutes),

    NgxsFormPluginModule,
    NgxsModule.forFeature([CategoryState]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [
    CategoriesActivator,
    CategoryService,
  ],
})
export class CategoriesModule {
}