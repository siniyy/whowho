import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  CategoryRedirect,
  CreateCategory,
  CreateCategorySuccess,
  CreateCategoryFailure,
  LoadCategories,
  LoadCategoriesSuccesss,
  RemoveCategory,
  RemoveCategorySuccess,
  SaveCategory,
  SaveCategoryFailure,
  SaveCategorySuccess,
} from './category.actions';
import { catchError, tap } from 'rxjs/internal/operators';
import { CategoryService } from '@app/categories/services/category.service';

import { AddForReload, ShowError, ShowSuccess } from '@app/shared/store/app.actions';
import { Navigate } from '@ngxs/router-plugin';
import { Category } from '@app/categories/models/category.model';

export interface CategoryStateModel {
  list: Category[];
}

export const CATEGORY_INITIAL_STATE = {
  list: null,
};

@State<CategoryStateModel>({
  name: 'categories',
  defaults: CATEGORY_INITIAL_STATE,
})
export class CategoryState {
  constructor(
    private categoryService: CategoryService,
  ) {}

  @Selector()
  static getList(state: CategoryStateModel) { return state.list; }

  @Action(LoadCategories)
  loadCategories({ dispatch }: StateContext<CategoryStateModel>) {
    return this.categoryService
      .getList()
      .pipe(
        tap(res => dispatch(new LoadCategoriesSuccesss(res.data))),
      );
  }

  @Action(LoadCategoriesSuccesss)
  loadCategoriesSuccess({ patchState }: StateContext<CategoryStateModel>, { payload }: LoadCategoriesSuccesss) {
    patchState({ list: payload })
  }

  @Action(RemoveCategory)
  removeCategory({ dispatch, patchState }: StateContext<CategoryStateModel>, { payload }: RemoveCategory) {
    return this.categoryService
      .remove(payload)
      .pipe(
        tap(() => dispatch(new RemoveCategorySuccess(payload))),
      );
  }

  @Action(RemoveCategorySuccess)
  removeCategorySuccess({ getState, patchState }: StateContext<CategoryStateModel>, { payload }: RemoveCategorySuccess) {
    const { list } = getState();

    patchState({
      list: list.filter(category => category._id !== payload),
    })
  }

  @Action(SaveCategory)
  saveCategory({ dispatch }: StateContext<CategoryStateModel>, { payload }: SaveCategory) {
    return this.categoryService
      .save(payload)
      .pipe(
        tap(res => dispatch(new SaveCategorySuccess(res.data))),
        catchError(error => dispatch(new SaveCategorySuccess(error))),
      );
  }

  @Action(SaveCategorySuccess)
  saveCategorySuccess({ dispatch, getState, patchState }: StateContext<CategoryStateModel>, { payload }: SaveCategorySuccess) {
    const { list } = getState();

    patchState({
      list: list.map(i => i._id === payload._id ? payload : i),
    });

    dispatch(new CategoryRedirect());
    dispatch(new ShowSuccess('Successfully saved'));
  }

  @Action(SaveCategoryFailure)
  saveCategoryFailure({ dispatch }: StateContext<CategoryStateModel>, { payload }: SaveCategoryFailure) {
    dispatch(new ShowError(payload));
  }

  @Action(CreateCategory)
  createCategory({ dispatch }: StateContext<CategoryStateModel>, { payload, shouldReset }: CreateCategory) {
    return this.categoryService
      .create(payload)
      .pipe(
        tap(res => dispatch(new CreateCategorySuccess(res.data, shouldReset))),
        catchError(error => dispatch(new SaveCategoryFailure(error))),
      );
  }

  @Action(CreateCategorySuccess)
  createCategorySuccess({ dispatch, getState, patchState }: StateContext<CategoryStateModel>, { payload, shouldReset }: CreateCategorySuccess) {
    const { list } = getState();

    list.push(payload);

    patchState({ list });

    if (!shouldReset) {
      dispatch(new CategoryRedirect());
    }

    dispatch(new ShowSuccess('Successfully added'));
  }

  @Action(CreateCategoryFailure)
  createCategoryFailure({ dispatch }: StateContext<CategoryStateModel>, { payload }: CreateCategoryFailure) {
    dispatch(new ShowError(payload));
  }

  @Action(CategoryRedirect)
  categoryRedirect({ dispatch }: StateContext<CategoryStateModel>) {
    dispatch(new Navigate(['/admin/categories']));
  }
}
