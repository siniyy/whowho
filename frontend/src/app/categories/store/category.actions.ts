import { Category } from '@app/categories/models/category.model';

export class LoadCategories {
  static readonly type = '[Categories] Load';
}

export class LoadCategoriesSuccesss {
  static readonly type = '[Categories] Load Success';
  constructor(public payload: Category[]) {}
}

export class LoadCategoriesFailure {
  static readonly type = '[Categories] Load Failure';
  constructor(public payload: any) {}
}

export class SaveCategory {
  static readonly type = '[Categories] Save';
  constructor(public payload: Category) {}
}

export class SaveCategorySuccess {
  static readonly type = '[Categories] Save Success';
  constructor(public payload) {}
}

export class SaveCategoryFailure {
  static readonly type = '[Categories] Save Failure';
  constructor(public payload: any) {}
}

export class CreateCategory {
  static readonly type = '[Categories] Create';
  constructor(public payload: Category, public shouldReset: boolean) {}
}

export class CreateCategorySuccess {
  static readonly type = '[Categories] Create Success';
  constructor(public payload: Category, public shouldReset: boolean) {}
}

export class CreateCategoryFailure {
  static readonly type = '[Categories] Create Failure';
  constructor(public payload: any) {}
}

export class RemoveCategory {
  static readonly type = '[Categories] Remove';
  constructor(public payload: string) {}
}

export class RemoveCategorySuccess {
  static readonly type = '[Categories] Remove  Success';
  constructor(public payload: string) {}
}

export class CategoryRedirect {
  static readonly type = '[Categories] Redirect to list';
}
