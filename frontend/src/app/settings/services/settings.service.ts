import { Injectable } from '@angular/core';
import { HttpService } from '@app/shared/services/http/http.service';

@Injectable()
export class SettingsService {
  url = 'admin/settings';

  constructor(private httpService: HttpService) {}

  getList() {
    return this.httpService.get(this.url);
  }

  create(category) {
    return this.httpService.post(this.url, category);
  }

  save(category) {
    return this.httpService.put(`${this.url}/${category.get('_id')}`, category);
  }

  remove(id) {
    return this.httpService.delete(`${this.url}/${id}`)
  }
}