import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { SettingsState } from '@app/settings/store/settings.state';
import { filter, switchMap, tap } from 'rxjs/internal/operators';
import { Observable, of } from 'rxjs/index';
import { LoadCategories } from '@app/categories/store/category.actions';
import { Settings } from '@app/settings/models/settings.model';

@Injectable()
export class SettingsActivator implements CanActivate {
  constructor(private store: Store) {}

  waitForDataToLoad(): Observable<Settings> {
    return this.store
      .select(SettingsState.getData)
      .pipe(
        filter(list => !!list),
      );
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.store.dispatch(new LoadCategories());

    return this
      .waitForDataToLoad()
      .pipe(
        switchMap(() => of(true)),
      );
  }
}