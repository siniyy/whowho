import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-settings-list-page',
  templateUrl: './moderators.component.html',
  styleUrls: ['./moderators.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModeratorsComponent {
  constructor() {}
}
