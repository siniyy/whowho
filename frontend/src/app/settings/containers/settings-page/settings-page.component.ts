import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';

import { SettingsState } from '@app/settings/store/settings.state';

@Component({
  selector: 'app-settings-list-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsPageComponent {
  @Select(SettingsState.getData) data$;

  constructor(private store: Store) {}
}
