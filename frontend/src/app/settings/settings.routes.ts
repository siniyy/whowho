import { Routes } from '@angular/router';
import { SettingsPageComponent } from '@app/settings/containers/settings-page/settings-page.component';
import { SettingsActivator } from '@app/settings/services/settings.activator';

export const settingsRoutes: Routes = [
  {
    path: '',
    canActivate: [SettingsActivator],
    component: SettingsPageComponent
  },
];
