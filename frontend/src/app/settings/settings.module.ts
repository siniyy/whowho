import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { MaterialModule } from '@app/shared/material';
import { ComponentsModule } from '@app/shared/components';

import { settingsRoutes } from '@app/settings/settings.routes';
import { SettingsState } from '@app/settings/store/settings.state';
import { CategoryService } from '@app/categories/services/category.service';
import { CategoriesActivator } from '@app/categories/services/categories.activator';
import { SettingsPageComponent } from '@app/settings/containers/settings-page/settings-page.component';
import { ModeratorsComponent } from '@app/settings/components/notificator-component/moderators.component';

export const COMPONENTS = [
  SettingsPageComponent,
  ModeratorsComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    ComponentsModule,
    RouterModule.forChild(settingsRoutes),

    NgxsFormPluginModule,
    NgxsModule.forFeature([SettingsState]),
  ],
  declarations: [...COMPONENTS],
  exports: COMPONENTS,
  providers: [
    CategoriesActivator,
    CategoryService,
  ],
})
export class SettingsModule {
}