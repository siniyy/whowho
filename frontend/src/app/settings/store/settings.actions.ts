import { Settings } from '@app/settings/models/settings.model';

export class LoadSettings {
  static readonly type = '[Settings] Load';
}

export class LoadSettingsSuccess {
  static readonly type = '[Settings] Load Success';
  constructor(public data: Settings) {}
}

export class SaveSettings {
  static readonly type = '[Settings] Save';
  constructor(public data: Settings) {}
}

export class SaveSettingsSuccess {
  static readonly type = '[Settings] Save Success';
  constructor(public data: Settings) {}
}