import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  LoadSettings,
  LoadSettingsSuccess,
  SaveSettings,
  SaveSettingsSuccess,
} from './settings.actions';
import { catchError, tap } from 'rxjs/internal/operators';

import { ShowSuccess } from '@app/shared/store/app.actions';
import { SettingsService } from '@app/settings/services/settings.service';
import { Settings } from '@app/settings/models/settings.model';

export interface SettingsStateModel {
  data: Settings;
}

export const SETTINGS_INITIAL_STATE = {
  data: null,
};

@State<SettingsStateModel>({
  name: 'settings',
  defaults: SETTINGS_INITIAL_STATE,
})
export class SettingsState {
  constructor(
    private settingsService: SettingsService,
  ) {}

  @Selector()
  static getData(state: SettingsStateModel) { return state.data; }

  @Action(LoadSettings)
  loadSettings({ dispatch }: StateContext<SettingsStateModel>) {
    return this.settingsService
      .getList()
      .pipe(
        tap(res => dispatch(new LoadSettingsSuccess(res.data))),
      );
  }

  @Action(LoadSettingsSuccess)
  loadSettingsSuccess({ patchState }: StateContext<SettingsStateModel>, { data }: LoadSettingsSuccess) {
    patchState({ data });
  }

  @Action(SaveSettings)
  saveSettings({ dispatch }: StateContext<SettingsStateModel>, { data }: SaveSettings) {
    return this.settingsService
      .save(data)
      .pipe(
        tap(res => dispatch(new SaveSettingsSuccess(res.data))),
        catchError(error => dispatch(new SaveSettingsSuccess(error))),
      );
  }

  @Action(SaveSettingsSuccess)
  saveCategorySuccess({ dispatch, getState, patchState }: StateContext<SettingsStateModel>, { data }: SaveSettingsSuccess) {
    patchState({ data });

    dispatch(new ShowSuccess('Successfully saved'));
  }
}
